USE [UserDB]
GO
/****** Object:  Table [dbo].[AirTransport]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AirTransport](
	[Item_ID] [int] NOT NULL,
	[ItemCategory_ID] [int] NOT NULL,
	[Brand_ID] [int] NOT NULL,
	[EngineType] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[NumberOfEngine] [int] NULL,
	[BulkMotor] [float] NULL,
	[EngineCapacity] [float] NULL,
	[MileageInHours] [float] NULL,
	[MaxSpeed] [float] NULL,
	[NumberOfSeats] [int] NULL,
	[Color] [varchar](50) NULL,
	[Weigth] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BankAccount]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BankAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](50) NOT NULL,
	[BankName] [varchar](50) NOT NULL,
	[Balance] [money] NOT NULL,
 CONSTRAINT [PK_BankAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Brands]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brands](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Brands] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Item]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Owner_ID] [int] NOT NULL,
	[IsUsed] [bit] NOT NULL,
	[Status] [bit] NOT NULL,
	[Header] [varchar](100) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[Price] [money] NOT NULL,
	[YearMade] [datetime] NOT NULL,
	[Views] [int] NOT NULL,
	[ItemCategory_ID] [int] NOT NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ItemCategory]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ItemCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RecoverySecret]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RecoverySecret](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Question] [varchar](200) NOT NULL,
	[Answer] [varchar](200) NOT NULL,
 CONSTRAINT [PK_RecoverySecret] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SpecialTransport]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SpecialTransport](
	[Item_ID] [int] NOT NULL,
	[ItemCategory_ID] [int] NOT NULL,
	[Brand_ID] [int] NOT NULL,
	[FuelType] [varchar](50) NULL,
	[DriveUnit] [varchar](50) NULL,
	[BulkMotor] [float] NULL,
	[EngineCapacity] [float] NULL,
	[Mileage] [float] NULL,
	[MaxSpeed] [float] NULL,
	[NumberOfSeats] [int] NULL,
	[Carrying] [float] NULL,
	[Color] [varchar](50) NULL,
	[Weigth] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserAdress]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAdress](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StreetName] [varchar](50) NOT NULL,
	[HouseNumber] [smallint] NOT NULL,
	[HouseCharacter] [char](10) NOT NULL,
	[PostalCode] [int] NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Municipality] [varchar](50) NOT NULL,
	[Province] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserAdress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsUser] [bit] NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Age] [tinyint] NULL,
	[PhoneNumber] [varchar](50) NULL,
	[Email] [varchar](50) NOT NULL,
	[CompanyName] [varchar](50) NOT NULL,
	[UserAdress_ID] [int] NULL,
	[RecoverySecret_ID] [int] NULL,
	[BankAccount_ID] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Vehicle]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vehicle](
	[Item_ID] [int] NOT NULL,
	[ItemCategory_ID] [int] NOT NULL,
	[Brand_ID] [int] NOT NULL,
	[DriveUnit] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[BulkMotor] [float] NULL,
	[EngineCapacity] [float] NULL,
	[Mileage] [float] NULL,
	[MaxSpeed] [float] NULL,
	[NumberOfSeats] [int] NULL,
	[Color] [varchar](50) NULL,
	[Weigth] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WaterTransport]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WaterTransport](
	[Item_ID] [int] NOT NULL,
	[ItemCategory_ID] [int] NOT NULL,
	[Brand_ID] [int] NOT NULL,
	[FuelType] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[BulkMotor] [float] NULL,
	[EngineCapacity] [float] NULL,
	[Mileage] [float] NULL,
	[MaxSpeed] [float] NULL,
	[BodyLength] [int] NULL,
	[NumberOfSeats] [int] NULL,
	[Color] [varchar](50) NULL,
	[Weigth] [float] NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Item] ON 

INSERT [dbo].[Item] ([Id], [Owner_ID], [IsUsed], [Status], [Header], [Description], [Price], [YearMade], [Views], [ItemCategory_ID]) VALUES (1, 1, 1, 1, N'Magic car', N'Very beautiful car', 755755.0000, CAST(N'1999-07-13T00:00:00.000' AS DateTime), 0, 1)
SET IDENTITY_INSERT [dbo].[Item] OFF
SET IDENTITY_INSERT [dbo].[ItemCategory] ON 

INSERT [dbo].[ItemCategory] ([Id], [Description]) VALUES (1, N'Vehicle')
INSERT [dbo].[ItemCategory] ([Id], [Description]) VALUES (2, N'Air transport')
INSERT [dbo].[ItemCategory] ([Id], [Description]) VALUES (3, N'Water transport')
INSERT [dbo].[ItemCategory] ([Id], [Description]) VALUES (4, N'Special transport')
INSERT [dbo].[ItemCategory] ([Id], [Description]) VALUES (5, N'Other item')
SET IDENTITY_INSERT [dbo].[ItemCategory] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [IsUser], [Login], [Password], [FirstName], [LastName], [Age], [PhoneNumber], [Email], [CompanyName], [UserAdress_ID], [RecoverySecret_ID], [BankAccount_ID]) VALUES (1, 1, N'user1', N'111111', N'user', N'one', 32, N'1224124124', N'user1@mail.com', N'ONE LTD', NULL, NULL, NULL)
INSERT [dbo].[Users] ([Id], [IsUser], [Login], [Password], [FirstName], [LastName], [Age], [PhoneNumber], [Email], [CompanyName], [UserAdress_ID], [RecoverySecret_ID], [BankAccount_ID]) VALUES (2, 1, N'user2', N'222222', N'user', N'two', NULL, NULL, N'user2@mail.com', N'TWO LTD', NULL, NULL, NULL)
INSERT [dbo].[Users] ([Id], [IsUser], [Login], [Password], [FirstName], [LastName], [Age], [PhoneNumber], [Email], [CompanyName], [UserAdress_ID], [RecoverySecret_ID], [BankAccount_ID]) VALUES (3, 1, N'user4', N'444444', N'user', N'four', NULL, NULL, N'user4@mail.com', N'FOUR LTD', NULL, NULL, NULL)
INSERT [dbo].[Users] ([Id], [IsUser], [Login], [Password], [FirstName], [LastName], [Age], [PhoneNumber], [Email], [CompanyName], [UserAdress_ID], [RecoverySecret_ID], [BankAccount_ID]) VALUES (4, 1, N'user3', N'333333', N'user', N'three', NULL, NULL, N'user3@mail.ru', N'THREE LTD', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[AirTransport]  WITH CHECK ADD  CONSTRAINT [FK_AirTransport_Brands] FOREIGN KEY([Brand_ID])
REFERENCES [dbo].[Brands] ([Id])
GO
ALTER TABLE [dbo].[AirTransport] CHECK CONSTRAINT [FK_AirTransport_Brands]
GO
ALTER TABLE [dbo].[AirTransport]  WITH CHECK ADD  CONSTRAINT [FK_AirTransport_Item] FOREIGN KEY([Item_ID])
REFERENCES [dbo].[Item] ([Id])
GO
ALTER TABLE [dbo].[AirTransport] CHECK CONSTRAINT [FK_AirTransport_Item]
GO
ALTER TABLE [dbo].[AirTransport]  WITH CHECK ADD  CONSTRAINT [FK_AirTransport_ItemCategory] FOREIGN KEY([ItemCategory_ID])
REFERENCES [dbo].[ItemCategory] ([Id])
GO
ALTER TABLE [dbo].[AirTransport] CHECK CONSTRAINT [FK_AirTransport_ItemCategory]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ItemCategory] FOREIGN KEY([ItemCategory_ID])
REFERENCES [dbo].[ItemCategory] ([Id])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ItemCategory]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Users] FOREIGN KEY([Owner_ID])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Users]
GO
ALTER TABLE [dbo].[SpecialTransport]  WITH CHECK ADD  CONSTRAINT [FK_SpecialTransport_Brands] FOREIGN KEY([Brand_ID])
REFERENCES [dbo].[Brands] ([Id])
GO
ALTER TABLE [dbo].[SpecialTransport] CHECK CONSTRAINT [FK_SpecialTransport_Brands]
GO
ALTER TABLE [dbo].[SpecialTransport]  WITH CHECK ADD  CONSTRAINT [FK_SpecialTransport_Item] FOREIGN KEY([Item_ID])
REFERENCES [dbo].[Item] ([Id])
GO
ALTER TABLE [dbo].[SpecialTransport] CHECK CONSTRAINT [FK_SpecialTransport_Item]
GO
ALTER TABLE [dbo].[SpecialTransport]  WITH CHECK ADD  CONSTRAINT [FK_SpecialTransport_ItemCategory] FOREIGN KEY([ItemCategory_ID])
REFERENCES [dbo].[ItemCategory] ([Id])
GO
ALTER TABLE [dbo].[SpecialTransport] CHECK CONSTRAINT [FK_SpecialTransport_ItemCategory]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_BankAccount] FOREIGN KEY([BankAccount_ID])
REFERENCES [dbo].[BankAccount] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_BankAccount]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_RecoverySecret] FOREIGN KEY([RecoverySecret_ID])
REFERENCES [dbo].[RecoverySecret] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_RecoverySecret]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserAdress] FOREIGN KEY([UserAdress_ID])
REFERENCES [dbo].[UserAdress] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_UserAdress]
GO
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_Brands] FOREIGN KEY([Brand_ID])
REFERENCES [dbo].[Brands] ([Id])
GO
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_Vehicle_Brands]
GO
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_Item] FOREIGN KEY([Item_ID])
REFERENCES [dbo].[Item] ([Id])
GO
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_Vehicle_Item]
GO
ALTER TABLE [dbo].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_ItemCategory] FOREIGN KEY([ItemCategory_ID])
REFERENCES [dbo].[ItemCategory] ([Id])
GO
ALTER TABLE [dbo].[Vehicle] CHECK CONSTRAINT [FK_Vehicle_ItemCategory]
GO
ALTER TABLE [dbo].[WaterTransport]  WITH CHECK ADD  CONSTRAINT [FK_WaterTransport_Brands] FOREIGN KEY([Brand_ID])
REFERENCES [dbo].[Brands] ([Id])
GO
ALTER TABLE [dbo].[WaterTransport] CHECK CONSTRAINT [FK_WaterTransport_Brands]
GO
ALTER TABLE [dbo].[WaterTransport]  WITH CHECK ADD  CONSTRAINT [FK_WaterTransport_Item] FOREIGN KEY([Item_ID])
REFERENCES [dbo].[Item] ([Id])
GO
ALTER TABLE [dbo].[WaterTransport] CHECK CONSTRAINT [FK_WaterTransport_Item]
GO
ALTER TABLE [dbo].[WaterTransport]  WITH CHECK ADD  CONSTRAINT [FK_WaterTransport_ItemCategory] FOREIGN KEY([ItemCategory_ID])
REFERENCES [dbo].[ItemCategory] ([Id])
GO
ALTER TABLE [dbo].[WaterTransport] CHECK CONSTRAINT [FK_WaterTransport_ItemCategory]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetFourRandomItems]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetFourRandomItems]
	@ItemsQuantity int
AS
	SELECT TOP (@ItemsQuantity) * FROM Item ORDER BY NEWID()


GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUser]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertUser]
	@login varchar(50),
	@password varchar(50),
	@firstname varchar(50),
	@lastname varchar(50),
	@email varchar(50),
	@companyname varchar(50)
AS
	INSERT INTO Users (Login, Password, FirstName, LastName, Email, CompanyName, IsUser)

	VALUES (@login, @password, @firstname, @lastname, @email, @companyname, 'true')

	SELECT SCOPE_IDENTITY()




GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePassword]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_UpdatePassword]
	@password varchar(50),
	@id int
AS
	Select top 1 * From Users where Id= @id;
	  
	  UPDATE Users
	  Set Password = @password

GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 08.12.2016 20:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_UpdateUser]
	@firstName varchar(50),
	@lastName varchar(50),
	@organization varchar(50),
	@phoneNumber varchar(50),
	@streetName varchar(50),
	@houseNumber varchar(50),
	@houseCharacter varchar(50),
	@postalCode varchar(50),
	@city varchar(50),
	@municipality varchar(50),
	@province varchar(50),
	@country varchar(50),
	@bankAccountNumber varchar(50),
	@bankName varchar(50),
	@isUser bit,
	@id varchar(50)

AS
	declare @addrId int;
	declare @accId int;

      Select top 1 @addrId=UserAdress_ID ,@accId =BankAccount_ID from Users where Id= @id;
	  
	  UPDATE Users
	  Set FirstName = @firstName,
	  LastName = @lastName,
	  CompanyName = @organization,
	  PhoneNumber = @phoneNumber,
	  IsUser = @isUser
      WHERE Id = @id;

	  if @addrId is not null
	  begin
		  UPDATE UserAdress
		  Set StreetName = @streetname,
		  HouseNumber = @houseNumber,
		  HouseCharacter = @houseCharacter,
		  PostalCode = @postalCode,
		  City = @city,
		  Municipality = @municipality,
		  Province = @province,
		  Country = @country
		  WHERE Id = @addrId;
	  end	
	  
	  if @accId is not null
	  begin
		  UPDATE BankAccount
		  Set BankName = @bankName,
		  AccountNumber = @bankAccountNumber
		  WHERE Id = @accId;
	  end

GO
