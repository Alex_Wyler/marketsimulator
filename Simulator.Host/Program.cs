﻿using MarketSimulator.Entities;
using Simulator.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities.DataBase_Entities;

namespace Simulator.Host
{
    class Program
    {
        static void Main(string[] args)
        {
            Simulation simulation = new Simulation(new List<User>());

            simulation.GenerateNPC(1);

            Console.ReadLine();
        }
    }
}
