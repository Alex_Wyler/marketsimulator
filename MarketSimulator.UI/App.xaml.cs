﻿using log4net;
using log4net.Config;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using MarketSimulator.BL;
using MarketSimulator.UI.View;

namespace MarketSimulator.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
     
        public App()
        {
            XmlConfigurator.Configure();
            WindowManager.OpenWindow(WindowName.Login, WindowShowType.Show, new LoginViewModel() { usermanager = new UserManager() { RegisterManager = new RegisterManager(), ItemManager = new ItemManager()}});
        }
    }
}