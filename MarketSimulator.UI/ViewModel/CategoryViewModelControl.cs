﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.Entities.Memory_Enntites;
using MarketSimulator.UI.Helpers;


namespace MarketSimulator.UI.ViewModel
{
    public class CategoryViewModelControl : BaseViewModel
    {
        public List<TransferCategory> TransferCategoryList { get; set; }

        public TransferCategory TransferCategory { get; set; }

        public ICommand categoryControlLoadedCommand { get; set; }

        public CategoryViewModelControl()
        {
            categoryControlLoadedCommand = new Command(arg => CategoryControlLoadedMethod());
        }


        private string _countLabel, _ownerLabel;

      
        public string ownerLabel
        {
            get { return _ownerLabel; }
            set
            {
                _ownerLabel = value;
                OnPropertyChanged("ownerLabel");
            }
        }

       
        public string countLabel
        {
            get { return _countLabel; }
            set
            {
                _countLabel = value;
                OnPropertyChanged("countLabel");
            }
        }


        public void CategoryControlLoadedMethod()
        {
            throw new NotImplementedException();
        }





    }
}
