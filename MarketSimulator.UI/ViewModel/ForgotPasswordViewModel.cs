﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using System.Text.RegularExpressions;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
{
    class ForgotPasswordViewModel:BaseViewModel
    {
        private bool _enableClickButtonConfirmSecretQuestion, _enableClickButtonGetQuestion, _enableAnswerTextBox, _enableClickButtonSend, _enableCodeConfirmButtonSend;
        private string _emailTextBoxValue,_codeFromEmailTextBoxValue,_secretAnswerTextBoxValue,
            _secretQuestionLabelBoxValue, _loginTextBoxValue;

        private RecoverySecret recoverySecret;


#region Properties
public bool enableCodeConfirmButtonSend
            {
            get { return _enableCodeConfirmButtonSend; }
            set
                {
                _enableCodeConfirmButtonSend = value;
                OnPropertyChanged("enableCodeConfirmButtonSend");
                }
            }

        public bool enableClickButtonSend
            {
            get { return _enableClickButtonSend; }
            set
                {
                _enableClickButtonSend = value;
                OnPropertyChanged("enableClickButtonSend");
                }
            }
        public bool enableClickButtonGetQuestion
        {
            get { return _enableClickButtonGetQuestion; }
            set
            {
                _enableClickButtonGetQuestion = value;
                OnPropertyChanged("enableClickButtonGetQuestion");
            }
        }


        public bool enableAnswerTextBox
        {
            get { return _enableAnswerTextBox; }
            set
            {
                _enableAnswerTextBox = value;
                OnPropertyChanged("enableAnswerTextBox");
            }
        }
        public bool enableClickButtonConfirmSecretQuestion
        {
            get { return _enableClickButtonConfirmSecretQuestion; }
            set
            {
                _enableClickButtonConfirmSecretQuestion = value;
                OnPropertyChanged("enableClickButtonConfirmSecretQuestion");
            }
        }
        public string emailTextBoxValue
        {
        get { return _emailTextBoxValue; }
            set
            {
            _emailTextBoxValue = value;
            OnPropertyChanged("emailTextBoxValue");
            }
        }
        public string codeFromEmailTextBoxValue
        {
        get { return _codeFromEmailTextBoxValue; }
            set
            {
            _codeFromEmailTextBoxValue = value;
            OnPropertyChanged("codeFromEmailTextBoxValue");
            }
        }  
        public string secretAnswerTextBoxValue
        {
        get { return _secretAnswerTextBoxValue; }
            set
            {
            _secretAnswerTextBoxValue = value;
            OnPropertyChanged("secretAnswerTextBoxValue");
            }
        }
        public string secretQuestionLabelBoxValue
        {
            get { return _secretQuestionLabelBoxValue; }
            set
            {
                _secretQuestionLabelBoxValue = value;
                OnPropertyChanged("secretQuestionLabelBoxValue");
            }
        }
        public string LoginTextBoxValue
        {
            get { return _loginTextBoxValue; }
            set
            {
                _loginTextBoxValue = value;
                OnPropertyChanged("LoginTextBoxValue");
            }
        }


        public ICommand sendCodeToEmailCommand { get; set; }
        public ICommand confirmCodeFromMailCommand { get; set; }
        public ICommand confirmSecretQuestionCommand { get; set; }
        public ICommand crossClosingCommand { get; set; }
        public ICommand LoginTextChangeCommand { get; set; }
        public ICommand EmailTextChangeCommand { get; set; }
        public ICommand getQuestionCommand { get; set; }
        public ICommand AnswerTextBoxChangedCommand { get; set; } 
        public ICommand EmailTextChangedCommand { get; set; }
        public ICommand codeChangeCommand { get; set; }
        
#endregion

        public ForgotPasswordViewModel()
        {
            sendCodeToEmailCommand = new Command(arg => SendCodeToEmailMethod());
            confirmCodeFromMailCommand = new Command(arg => ConfirmCodeFromMailMethod());
            confirmSecretQuestionCommand = new Command(arg => ConfirmSecretQuestionMethod());
            getQuestionCommand=new Command(arg => GetQuestionMethod());
            crossClosingCommand = new Command(arg => CrossClosing());
            EmailTextChangeCommand = new Command(arg => EmailTextBoxChangeMethod());
            codeChangeCommand = new Command(arg => CodeChangeMethod());
            LoginTextChangeCommand = new Command(arg => LoginTextBoxChangeMethod());
           AnswerTextBoxChangedCommand=new Command(arg=> AnswerTextBoxChangedMethod());
           enableClickButtonConfirmSecretQuestion = false;
            enableAnswerTextBox = false;

        }
        private void AnswerTextBoxChangedMethod()
        {
            if (secretAnswerTextBoxValue.Length > 0)
            {
                enableClickButtonConfirmSecretQuestion = true;
            }
            else
                enableClickButtonConfirmSecretQuestion = false;
        }

        private void EmailTextBoxChangeMethod()
        {
          
        if (!String.IsNullOrWhiteSpace(emailTextBoxValue)) enableClickButtonSend = true;
            
        else enableClickButtonSend = false;
        }

        private void CodeChangeMethod()
            {

            if (!String.IsNullOrWhiteSpace(codeFromEmailTextBoxValue)) enableCodeConfirmButtonSend = true;

            else enableCodeConfirmButtonSend = false;
            }



        private void GetQuestionMethod()
        {
            try
            {
                recoverySecret = usermanager.GetSecredQuestion(LoginTextBoxValue);
                secretQuestionLabelBoxValue = recoverySecret.Question;
                enableAnswerTextBox = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Error: unable to retrieve recovery secret");
            }
        }
        

        

        #region Methods
        public void LoginTextBoxChangeMethod()
        {
            if (LoginViewModel.CheckLoginTextBox(LoginTextBoxValue))
            {
                enableClickButtonGetQuestion = true; ;
            }
            else
                enableClickButtonGetQuestion = false;
        }
        public void SendCodeToEmailMethod()
        {
            MessageBox.Show(usermanager.SendEmailRecovery(emailTextBoxValue)
                ? "Message sent to your e-mail"
                : "No user registered with this email");
        }

        public void ConfirmCodeFromMailMethod()
        {

            string codepatern = @"[0-9]{5}";
            string WrongCodeError = "Error: wrong e-mail code";
            if (Regex.IsMatch(codeFromEmailTextBoxValue,codepatern))
            {
                if (usermanager.CheckValidationCode(codeFromEmailTextBoxValue))
                {
                    WindowManager.OpenWindow(WindowName.Settings, WindowShowType.ShowDialog,
                        new SettingsWindowViewModel(true)
                        {
                            usermanager = usermanager
                        });

                    WindowManager.CloseWindow(WindowName.ForgotPassword);
                    WindowManager.Disapose(WindowName.ForgotPassword);
                    WindowManager.CloseWindow(WindowName.Login);
                    WindowManager.Disapose(WindowName.Login);
                }
                else
                {
                    MessageBox.Show(WrongCodeError);
                }
            }
            else
            {
                MessageBox.Show(WrongCodeError);
            }

        }

        public void ConfirmSecretQuestionMethod()
        {
            if (secretAnswerTextBoxValue == recoverySecret.Answer)
            {
                var result = usermanager.Login(LoginTextBoxValue);

                if (result.errorsList.Count == 0)
                {
                    WindowManager.OpenWindow(WindowName.Settings, WindowShowType.ShowDialog,
                    new SettingsWindowViewModel(true)
                    {
                        usermanager = usermanager
                    });
                    WindowManager.CloseWindow(WindowName.ForgotPassword);
                    WindowManager.Disapose(WindowName.ForgotPassword);
                    WindowManager.CloseWindow(WindowName.Login);
                    WindowManager.Disapose(WindowName.Login);
                }
                else
                {
                    MessageBox.Show(result.errorsList[0].Message);
                }
            }
        }

        public void CrossClosing()
        {
            WindowManager.Disapose(WindowName.ForgotPassword);
        }


#endregion
    }
}
