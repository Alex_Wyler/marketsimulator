﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel.FrameViewModel
{
    public class MarketFrameViewModel : BaseViewModel
    {



        public MarketFrameViewModel()
        {
            MarketFrameLoadedCommand = new Command(arg => MarketFrameLoadedMethod());
            returnButtonClickCommand = new Command(arg => ReturnItemsMethod());
            addItemButtonClickCommand = new Command(arg => OpenAddItemWindowMethod());
            clearCommand = new Command(arg => ClearFiltersMethod());
            applyCommand = new Command(arg => ApplyFiltersMethod());
            minTextChangedCommand = new Command(arg => MinTextChangedMethod());
            maxTextChangedCommand = new Command(arg => MaxTextChangedMethod());
            


            //minFilter = "min price";
            //maxFilter = "max price";
            Size = 3;
            GetAllItems();
            //AddToList addToList = AddItemRefresh;

        }


        public ICommand MarketFrameLoadedCommand { get; set; }
        public ICommand returnButtonClickCommand { get; set; }
        public ICommand addItemButtonClickCommand { get; set; }
        public ICommand clearCommand { get; set; }
        public ICommand applyCommand { get; set; }
        public ICommand minTextChangedCommand { get; set; }
        public ICommand maxTextChangedCommand { get; set; }
        public delegate void UpdateListDelegate(int id);

        public delegate void AddToList(Item item);

        private bool _enableFiltersGrid;

        public int Size { get; set; }
        public List<Item> AllItemList;
        public ObservableCollection<UserControl> _myShowItems = new ObservableCollection<UserControl>();
        private string numPattern = @"^[0-9]+$";
        private string _sm, _uniformGridChildren, _minFilter, _maxFilter, _selectedCategoryValue;
        private ItemCategory _itemComboBoxCategory;


        public string selectedCategoryValue
            {
            get { return _selectedCategoryValue; }
            set
                {
                _selectedCategoryValue = value;
                OnPropertyChanged("selectedCategoryValue");
                }
            }
        
        public IEnumerable<ItemCategory> itemComboBoxCategory
        {
            get
            {
                return Enum.GetValues(typeof(ItemCategory))
                    .Cast<ItemCategory>();
            }
        }



        public string maxFilter
        {
            get { return _maxFilter; }
            set
            {
                _maxFilter = value;
                OnPropertyChanged("maxFilter");
            }
        }

        public string minFilter
        {
            get { return _minFilter; }
            set
            {
                _minFilter = value;
                OnPropertyChanged("minFilter");
            }
        }

        public bool enableFiltersGrid
        {
            get { return _enableFiltersGrid; }
            set
            {
                _enableFiltersGrid = value;
                OnPropertyChanged("enableFiltersGrid");
            }
        }

        public ObservableCollection<UserControl> MyShowItems
        {
            get { return _myShowItems; }
            set
            {
                _myShowItems = value;
                OnPropertyChanged("MyShowItems");
            }
        }

        public string SM
        {
            get { return _sm; }
            set
            {
                _sm = value;
                OnPropertyChanged("SM");
            }
        }

        public string UniformGridChildren
        {
            get { return _uniformGridChildren; }
            set
            {
                _uniformGridChildren = value;
                OnPropertyChanged("UniformGridChildren");
            }
        }

        public void MarketFrameLoadedMethod()
        {

            if (usermanager.User == null) enableFiltersGrid = false;
            if (usermanager.User != null) enableFiltersGrid = true;

        }

        public void GetAllItemsFromDb()
        {
            UpdateListDelegate updateList = UpdateItems;
            ItemManager itm = new ItemManager();
            AllItemList = itm.GetAllItems();
            foreach (var item in AllItemList)
            {
                ItemViewModelControl vm = new ItemViewModelControl(item, updateList)
                {
                    usermanager = WindowManager.MainVM.usermanager
                };

                MyShowItems.Add(new ItemViewControl
                {
                    DataContext = vm,
                    DescriptionLabel = {Text = item.Description},
                    HeaderLabel = {Content = item.Header},
                    PriceLabel = {Content = item.Price.ToString("0.##")},

                });



            }
        }

        public void AddItemRefresh(Item item)
        {
            MyShowItems.Clear();
            UpdateListDelegate updateList = UpdateItems;
            WindowManager.MainVM.usermanager.ItemManager._il.Add(item);
            foreach (var it in WindowManager.MainVM.usermanager.ItemManager._il)
            {
                ItemViewModelControl vm = new ItemViewModelControl(it, updateList)
                {
                    usermanager = WindowManager.MainVM.usermanager
                };

                MyShowItems.Add(new ItemViewControl
                {
                    DataContext = vm,
                    DescriptionLabel = {Text = it.Description},
                    HeaderLabel = {Content = it.Header},
                    PriceLabel = {Content = it.Price.ToString("0.##")},

                });
            }

        }

        public void UpdateItems(int Id)
        {


            var MyItems = from item in WindowManager.MainVM.usermanager.ItemManager._il
                where item.Id == Id
                select item;

            foreach (var item in MyItems)
            {
                WindowManager.MainVM.usermanager.ItemManager._il.Remove(item);
                break;
            }

            var MyControls = from control in MyShowItems
                where (control.DataContext as ItemViewModelControl).myModelItem.Id == Id
                select control;

            foreach (var control in MyControls)
            {
                MyShowItems.Remove(control);
                break;
            }


        }

        public void ReturnItemsMethod()
        {
            var itm = new ItemManager();

            if (itm.ReturnStatus() != -1)
            {
                MyShowItems.Clear();

                GetAllItemsFromDb();
                MessageBox.Show("All items returned to database.");
            }

            else
            {
                MessageBox.Show("Error while return items to database!");
            }

        }

        public void OpenAddItemWindowMethod()
        {
            AddToList addToList = AddItemRefresh;
            WindowManager.OpenWindow(WindowName.AddItem, WindowShowType.ShowDialog,
                new AddItemViewModel(addToList) {usermanager = usermanager});
        }

        public void GetAllItems()
        {
            UpdateListDelegate updateList = UpdateItems;
            if (WindowManager.MainVM.usermanager.ItemManager._il != null)
            {
                foreach (var item in WindowManager.MainVM.usermanager.ItemManager._il)
                {
                    if (item.IsValid)
                    {
                        ItemViewModelControl vm = new ItemViewModelControl(item, updateList)
                        {
                            usermanager = WindowManager.MainVM.usermanager
                        };

                        MyShowItems.Add(new ItemViewControl
                        {
                            DataContext = vm,
                            DescriptionLabel = { Text = item.Description },
                            HeaderLabel = { Content = item.Header },
                            PriceLabel = { Content = item.Price.ToString("0.##") },

                        });
                    }
                    



                }
            }
            else
            {

                MessageBox.Show("Error while loading database items");
            }
        }

        public void ClearFiltersMethod()
        {
            minFilter = string.Empty;
            maxFilter = string.Empty;
            selectedCategoryValue = string.Empty;

            var collection = usermanager.ItemManager._il;
            foreach (var item in collection)
            {
                item.IsValid = true;
            }
            MyShowItems.Clear();
            GetAllItems();
        }

        public void ApplyFiltersMethod()
        {

            var collection = usermanager.ItemManager._il;
            foreach (var item in collection)
            {
                item.IsValid = false;
            }


            if (!string.IsNullOrWhiteSpace(minFilter) && !string.IsNullOrWhiteSpace(maxFilter) &&
                !string.IsNullOrWhiteSpace(selectedCategoryValue)) // 123
            {
                foreach (
                    var item in
                        collection.Where(
                            item =>
                                item.Price >= Convert.ToDecimal(minFilter) && item.Price <= Convert.ToDecimal(maxFilter) &&
                                item.Category.ToString() == selectedCategoryValue))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (!string.IsNullOrWhiteSpace(minFilter) && !string.IsNullOrWhiteSpace(maxFilter) &&
                     string.IsNullOrWhiteSpace(selectedCategoryValue)) // 12
            {
                foreach (
                    var item in
                        collection.Where(
                            item =>
                                item.Price >= Convert.ToDecimal(minFilter) && item.Price <= Convert.ToDecimal(maxFilter))
                    )
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (!string.IsNullOrWhiteSpace(minFilter) && string.IsNullOrWhiteSpace(maxFilter) &&
                     !string.IsNullOrWhiteSpace(selectedCategoryValue)) // 13
            {
                foreach (var item in collection.Where(item => item.Price >= Convert.ToDecimal(minFilter) &&
                                                              item.Category.ToString() == selectedCategoryValue))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (string.IsNullOrWhiteSpace(minFilter) && !string.IsNullOrWhiteSpace(maxFilter) &&
                     !string.IsNullOrWhiteSpace(selectedCategoryValue)) //23
            {
                foreach (var item in collection.Where(item => item.Price <= Convert.ToDecimal(maxFilter) &&
                                                              item.Category.ToString() == selectedCategoryValue))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (!string.IsNullOrWhiteSpace(minFilter) && string.IsNullOrWhiteSpace(maxFilter) &&
                     string.IsNullOrWhiteSpace(selectedCategoryValue)) //1
            {
                foreach (var item in collection.Where(item => item.Price >= Convert.ToDecimal(minFilter)))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (string.IsNullOrWhiteSpace(minFilter) && !string.IsNullOrWhiteSpace(maxFilter) &&
                     string.IsNullOrWhiteSpace(selectedCategoryValue)) //2
            {
                foreach (var item in collection.Where(item => item.Price <= Convert.ToDecimal(maxFilter)))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }
            else if (string.IsNullOrWhiteSpace(minFilter) && string.IsNullOrWhiteSpace(maxFilter) &&
                     !string.IsNullOrWhiteSpace(selectedCategoryValue)) //3
            {
                foreach (var item in collection.Where(item =>
                    item.Category.ToString() == selectedCategoryValue))
                {
                    item.IsValid = true;
                }
                MyShowItems.Clear();
                GetAllItems();
            }

            if (string.IsNullOrWhiteSpace(selectedCategoryValue)) return;

            foreach (var category in itemComboBoxCategory.Where(category => category.ToString() == selectedCategoryValue))
            {
                itemmanager = new ItemManager();
                itemmanager.UpdateUserStory((int)(category), usermanager.User.Id);
            }
        }

        public void MinTextChangedMethod()
        {
        string nPattern = @"^[0-9]+$";

        if (!Regex.IsMatch(minFilter, nPattern))
        {
            minFilter = "";
        }
        else
            {
           
            }



        }
        public void MaxTextChangedMethod()
            {
            string nPattern = @"^[0-9]+$";

            if (!Regex.IsMatch(maxFilter, nPattern))
                {
                maxFilter = "";
                }
            else
                {

                }



            }
    }
}

