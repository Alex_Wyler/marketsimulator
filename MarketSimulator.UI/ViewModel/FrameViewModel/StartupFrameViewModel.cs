﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel.FrameViewModel
{
    public class StartupFrameViewModel : BaseViewModel
    {
        public delegate void UpdateStartUpListDelegate(int id);

        public StartupFrameViewModel(UserManager userManager)
        {
            usermanager = userManager;
            StartupFrameLoadedCommand = new Command(arg => StartupFrameLoadedMethod());
            StartupFrameGotFocusCommand = new Command(arg => StartupFrameGotFocusMethod());
            Size = 3;
            GetRandomItems();
        }

        public void GetRandomItems()
        {
            var itm = new ItemManager();
            ItemList = itm.GetRandomItems(usermanager.User?.Id ?? 0);

            UpdateStartUpListDelegate updateStartList = UpdateStartUpItems;

            foreach (var item in ItemList)
            {
                ItemViewModelControl vm = new ItemViewModelControl(item, updateStartList)
                {
                    usermanager = WindowManager.MainVM.usermanager
                };


                MyShowItems.Add(new ItemViewControl() //создаем контрол плитки с обїектом Итем
                {
                    DescriptionLabel = {Text = item.Description},
                    HeaderLabel = {Content = item.Header},
                    PriceLabel = {Content = item.Price.ToString("0.##")},
                    DataContext = vm
                });
            }

        }

        public ICommand StartupFrameLoadedCommand { get; set; }
        public ICommand StartupFrameGotFocusCommand { get; set; }
        public int Size { get; set; }
        public List<Item> ItemList;
        public ObservableCollection<UserControl> _myShowItems = new ObservableCollection<UserControl>();

        private string _sm;
        private string _uniformGridChildren;

        public ObservableCollection<UserControl> MyShowItems
        {
            get { return _myShowItems; }
            set
            {
                _myShowItems = value;
                OnPropertyChanged("MyShowItems");
            }
        }

        public string SM
        {
            get { return _sm; }
            set
            {
                _sm = value;
                OnPropertyChanged("SM");
            }
        }

        public string UniformGridChildren
        {
            get { return _uniformGridChildren; }
            set
            {
                _uniformGridChildren = value;
                OnPropertyChanged("UniformGridChildren");
            }
        }

        public void StartupFrameLoadedMethod()
        {

        }

        public void StartupFrameGotFocusMethod()
        {
            //GetRandomItems();

        }

        public void UpdateStartUpItems(int Id)
        {
        
            var MyItems = from item in ItemList
                where item.Id == Id
                select item;

            foreach (var item in MyItems)
            {
                WindowManager.MainVM.usermanager.ItemManager._il.Remove(item);
                break;
            }

            var MyControls = from control in MyShowItems
                where (control.DataContext as ItemViewModelControl).myModelItem.Id == Id
                select control;

            foreach (var control in MyControls)
            {
                MyShowItems.Remove(control);
                break;
            }
        }
    }
}
