﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using System.Text.RegularExpressions;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel.FrameViewModel;
using Microsoft.Win32;

namespace MarketSimulator.UI.ViewModel
{
    public class AddItemViewModel : BaseViewModel
    {

        #region Variables

        private bool _enableConfirmButton;

        private string _priceTextBoxValue;

        private string _headerTextBoxValue,
            _descriptionTextBoxValue;

        private DateTime _dateOfManufactureDateTime;

        private ItemCategory _categoryComboBoxValue;

        private Conditions _conditionComboBoxValue;

        private BitmapImage _imageView;

        private string pricePattern = @"^[0-9]+$";
        private MarketFrameViewModel.AddToList addItemDelegate;
        #endregion


        #region Properties

        public Byte[] Image { get; set; }

        public string HeaderTextBoxValue
        {
            get { return _headerTextBoxValue; }
            set
            {
                _headerTextBoxValue = value;
                OnPropertyChanged("HeaderTextBoxValue");
            }
        }

        public string DescriptionTextBoxValue
        {
            get { return _descriptionTextBoxValue; }
            set
            {
                _descriptionTextBoxValue = value;
                OnPropertyChanged("DescriptionTextBoxValue");
            }
        }

        public string PriceTextBoxValue
        {
            get { return _priceTextBoxValue; }
            set
            {
                _priceTextBoxValue = value;
                OnPropertyChanged("PriceTextBoxValue");
            }
        }

        public ICommand CategoryTextChangeCommand { get; set; }
        public ICommand HeaderTextChangedCommand { get; set; }
        public ICommand DescriptionTextChangedCommand { get; set; }
        public ICommand PriceTextChangedCommand { get; set; }
        public ICommand BrowseImageFile { get; set; }
        public ICommand AddNewItemCommand { get; set; }
        public ICommand CrossClosingCommand { get; set; }
        public ICommand ClosingCommand { get; set; }

        public ItemCategory SelectedCategoryComboBoxValue
        {
            get { return _categoryComboBoxValue; }
            set
            {
                _categoryComboBoxValue = value;
                OnPropertyChanged("SelectedCategoryComboBoxValue");
            }
        }

        public Conditions SelectedConditionComboBoxValue
        {
            get { return _conditionComboBoxValue; }
            set
            {
                _conditionComboBoxValue = value;
                OnPropertyChanged("SelectedConditionComboBoxValue");
            }
        }

        public DateTime DateManufactureSelectedDateChanged
        {
            get {  return _dateOfManufactureDateTime; }
            set
            {
                _dateOfManufactureDateTime = value;
                OnPropertyChanged("DateManufactureSelectedDateChanged");
            }
        }
        
        public IEnumerable<ItemCategory> ItemCategoriesValues
        {
            get
            {
                return Enum.GetValues(typeof (ItemCategory))
                    .Cast<ItemCategory>();
            }
        }

        public IEnumerable<Conditions> ItemConditionValues
        {
            get
            {
                return Enum.GetValues(typeof (Conditions))
                        .Cast<Conditions>();
            }
        }

        public BitmapImage ImageView
        {
            get { return _imageView; }
            set
            {
                _imageView = value;
                OnPropertyChanged("ImageView");
            }
        }

        public bool EnableConfirmButton
        {
            get { return _enableConfirmButton; }
            set
            {
                _enableConfirmButton = value;
                OnPropertyChanged("EnableConfirmButton");
            }
        }

        #endregion

        public AddItemViewModel(MarketFrameViewModel.AddToList addToList)
        {
            addItemDelegate = addToList;
            itemmanager = new ItemManager();
            HeaderTextChangedCommand = new Command(arg => HeaderTextBoxChangeMethod());
            DescriptionTextChangedCommand = new Command(arg => DescriptionTextBoxChangeMethod());
            PriceTextChangedCommand = new Command(arg => PriceTextBoxChangeMethod());
            BrowseImageFile = new Command(arg => AddImageFileMethod());
            AddNewItemCommand = new Command(arg => CreateNewItemMethod());
            CrossClosingCommand = new Command(arg => KillAddItemWindow());
            ClosingCommand = new Command(arg => ClosingAddItemWindow());
            SelectedCategoryComboBoxValue = ItemCategory.Other;
            SelectedConditionComboBoxValue = Conditions.New;
            DateManufactureSelectedDateChanged = DateTime.Now;
        }

        #region Methods

        private bool CheckAllInputs() {

            if (!String.IsNullOrWhiteSpace(HeaderTextBoxValue) && !String.IsNullOrWhiteSpace(DescriptionTextBoxValue) && !String.IsNullOrWhiteSpace(PriceTextBoxValue) && !(PriceTextBoxValue.StartsWith("0"))
                && Image != null && Regex.IsMatch(PriceTextBoxValue, pricePattern))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void HeaderTextBoxChangeMethod()
        {
            if (CheckAllInputs())
            {
                EnableConfirmButton = true;
            }
            else
            {
                EnableConfirmButton = false;
            }
        }

        public void DescriptionTextBoxChangeMethod()
        {
            if (CheckAllInputs())
            {
                EnableConfirmButton = true;
            }
            else
            {
                EnableConfirmButton = false;
            }
        }

        public void PriceTextBoxChangeMethod()
        {
           if (CheckAllInputs())
            {
                    EnableConfirmButton = true;
            }
            else
            {
                EnableConfirmButton = false;
            }
        }

        public void AddImageFileMethod()
        {
            try
            {
                var dialog = new OpenFileDialog
                {
                    DefaultExt = ".png",
                    Filter =
                        "Image files (*.jpeg, *.jpg, *.png, *.gif, *.bmp)|*.jpeg; *.jpg; *.png; *.gif; *.bmp|JPEG Files (*.jpeg)|*.jpeg|JPG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|GIF Files (*.gif)|*.gif|BMP Files (*.bmp)|*.bmp"
                };


                dialog.ShowDialog();

                ImageView = new BitmapImage();

                ImageView.BeginInit();
                ImageView.UriSource = new Uri(dialog.FileName);
                ImageView.EndInit();

                Image = BitmapImageConverter.ConvertToByteFromBitmapImage(ImageView);

                if (!String.IsNullOrWhiteSpace(HeaderTextBoxValue) && !String.IsNullOrWhiteSpace(DescriptionTextBoxValue) &&
                !String.IsNullOrWhiteSpace(PriceTextBoxValue) && PriceTextBoxValue != (0).ToString() && Image != null)
                {
                    EnableConfirmButton = true;
                }
                else
                {
                    EnableConfirmButton = false;
                }

            }
            catch (Exception)
            {


            }
        }

        public void CreateNewItemMethod()
        {
            Item newItem = new Item(usermanager.User, SelectedCategoryComboBoxValue,
                SelectedConditionComboBoxValue == Conditions.Used, HeaderTextBoxValue, DescriptionTextBoxValue,decimal.Parse(PriceTextBoxValue),
                DateManufactureSelectedDateChanged, true, Image);

            var result = itemmanager.AddNewItem(newItem);
            addItemDelegate(newItem);
            MessageBox.Show("Item successfully added!", "Message", MessageBoxButton.OK);
            WindowManager.CloseWindow(WindowName.AddItem);

        }

        public void KillAddItemWindow()
        {
            WindowManager.Disapose(WindowName.AddItem);
        }

        public void ClosingAddItemWindow()
        {
            WindowManager.CloseWindow(WindowName.AddItem);
            WindowManager.Disapose(WindowName.AddItem);
        }

        #endregion
    }
}
