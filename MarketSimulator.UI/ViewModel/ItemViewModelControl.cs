﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.View;
using MarketSimulator.UI.View.Frames;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.ViewModel
    {
    public class ItemViewModelControl : BaseViewModel
    {
        public List<Item> ItemList { get; set; }
        public Item myModelItem { get; set; }
        public ICommand itemControlLoadedCommand { get; set; }
        public ICommand buyButtonClickCommand { get; set; }
        public ICommand ownerButtonClickCommand { get; set; }
        MarketFrameViewModel.UpdateListDelegate uDel;
        StartupFrameViewModel.UpdateStartUpListDelegate uStDel;


        public ItemViewModelControl(Item item, StartupFrameViewModel.UpdateStartUpListDelegate updateStartUpListDelegate)
            {
            myModelItem = item;
            Image = new BitmapImage();
            uStDel = updateStartUpListDelegate;
            //var del = updateListDelegate;

            Image = BitmapImageConverter.ConvertToBitMapImageFromByte(myModelItem.Photo);

            itemControlLoadedCommand = new Command(arg => ItemControlLoadedMethod());
            buyButtonClickCommand = new Command(arg => BuyCallConfirmMethod());
            ownerButtonClickCommand = new Command(arg => OwnerMethod());
            }

        public ItemViewModelControl(Item item, MarketFrameViewModel.UpdateListDelegate updateListDelegate)
        {
            myModelItem = item;
            Image = new BitmapImage();
            uDel = updateListDelegate;
            
            Image = BitmapImageConverter.ConvertToBitMapImageFromByte(myModelItem.Photo);

            itemControlLoadedCommand = new Command(arg => ItemControlLoadedMethod());
            buyButtonClickCommand = new Command(arg => BuyCallConfirmMethod());
            ownerButtonClickCommand = new Command(arg =>OwnerMethod());
            }
        public ItemViewModelControl(Item item)
            {
            myModelItem = item;
            Image = new BitmapImage();

            Image = BitmapImageConverter.ConvertToBitMapImageFromByte(myModelItem.Photo);

            itemControlLoadedCommand = new Command(arg => ItemControlLoadedMethod());
            buyButtonClickCommand = new Command(arg => BuyCallConfirmMethod());
            ownerButtonClickCommand = new Command(arg => OwnerMethod());
            }
        
        private string _headerLabel, _priceLabel, _ownerLabel, _descriptionLabel;
        private bool _buyButtonEnable;
        private BitmapImage _imageView;


        public BitmapImage Image
        {
            get { return _imageView; }
            set
            {
                _imageView = value;
                OnPropertyChanged("Image");
            }
        }


        public bool buyButtonEnable
            {
            get { return _buyButtonEnable; }
            set
                {
                _buyButtonEnable = value;
                OnPropertyChanged("buyButtonEnable");
                }
            }

        public string descriptionLabel
            {
            get { return _descriptionLabel; }
            set
                {
                _descriptionLabel = value;
                OnPropertyChanged("descriptionLabel");
                }
            }

        public string ownerLabel
            {
            get { return _ownerLabel; }
            set
                {
                _ownerLabel = value;
                OnPropertyChanged("ownerLabel");
                }
            }
        
        public string headerLabel
            {
            get { return _headerLabel; }
            set
                {
                _headerLabel = value;
                OnPropertyChanged("headerLabel");
                }
            }
        public string priceLabel
            {
            get { return _priceLabel; }
            set
                {
                _priceLabel = value;
                OnPropertyChanged("priceLabel");
                }
            }

        public void ItemControlLoadedMethod()
        {
            if (usermanager.User!= null && usermanager.User.Login!=myModelItem.Owner.Login)
            {
                buyButtonEnable = true;
            }
            else
            {
                buyButtonEnable = false;
            }


        }

        public void BuyCallConfirmMethod()
        {
        if (uDel != null) {WindowManager.OpenWindow(WindowName.Buy, WindowShowType.ShowDialog, new BuyViewModel(myModelItem, usermanager, uDel));}
        if (uStDel !=null) {WindowManager.OpenWindow(WindowName.Buy, WindowShowType.ShowDialog, new BuyViewModel(myModelItem, usermanager, uStDel));} //передаем Итем в ВМ поп-ап окна

        }

        public void OwnerMethod()
            {
            WindowManager.OpenWindow(WindowName.OwnerInfo, WindowShowType.ShowDialog, new OwnerInfoViewModel(myModelItem));

            }
                
    
        }
  
    }
