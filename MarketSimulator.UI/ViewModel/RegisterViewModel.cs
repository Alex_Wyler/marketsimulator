﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MarketSimulator.UI.Helpers;
using System.Windows.Interactivity;
using System.Text.RegularExpressions;
using log4net;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.Entities.Memory_Enntites;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.ViewModel
    {
    class RegisterViewModel : BaseViewModel
    {
        private readonly ILog _regWindowLog = LogManager.GetLogger("[Registraton window]: ");

        private string _loginBoxValue,
            _passwordBoxValue,
            _emailBoxValue,
            _nameBoxValue,
            _surnameBoxValue,
            _organizationBoxValue,
            _squestionBoxValue,
            _sanswerBoxValue,
            _accountBoxValue,
            _loginValidationLabel,
            _loginValidationLabelColor,
            _emailValidationLabel,
            _emailValidationLabelColor,
            _passwordValidationLabel,
            _passwordValidationLabelColor,
            _accountValidationLabel,
            _accountValidationLabelColor;

        private bool _enableRegisterButton;

        public RegisterViewModel()
        {
        
        cancelButtonClickCommand = new Command(arg => ClickCancelMethod());
        registerButtonClickCommand = new Command(arg => ClickRegisterMethod());
        loginTextChangeCommand = new Command(arg => LoginTextBoxChangeMethod());
        emailTextChangeCommand = new Command(arg => EmailTextBoxChangeMethod());
        passwordTextChangeCommand = new Command(arg => PasswordTextBoxChangeMethod());
        nameTextChangeCommand = new Command(arg => NameTextBoxChangeMethod());
        surnameTextChangeCommand = new Command(arg => SurnameTextBoxChangeMethod());
        organizationTextChangeCommand = new Command(arg => OrganizationTextBoxChangeMethod());
        accountTextChangeCommand = new Command(arg => AccountTextBoxChangeMethod());
        squestionTextChangeCommand = new Command(arg => SquestionTextBoxChangeMethod());
        sanswerTextChangeCommand = new Command(arg => SanswerTextBoxChangeMethod());
        crossClosingCommand = new Command(arg => KillRegisterWindow());
        
        LoginCommand = new RelayCommand(Login);
        

        
        }

        #region Properties
        public ICommand cancelButtonClickCommand { get; set; }
        public ICommand registerButtonClickCommand { get; set; }
        public ICommand loginTextChangeCommand { get; set; }
        public ICommand emailTextChangeCommand { get; set; }
        public ICommand passwordTextChangeCommand { get; set; }
        public ICommand nameTextChangeCommand { get; set; }
        public ICommand surnameTextChangeCommand { get; set; }
        public ICommand organizationTextChangeCommand { get; set; }
        public ICommand accountTextChangeCommand { get; set; }
        public ICommand squestionTextChangeCommand { get; set; }
        public ICommand sanswerTextChangeCommand { get; set; }
        public ICommand crossClosingCommand { get; set; }

        
        public ICommand LoginCommand
            {
            get;
            private set;
            }

        public string accountValidationLabelColor
            {
            get { return _accountValidationLabelColor; }
            set
                {
                _accountValidationLabelColor = value;
                OnPropertyChanged("accountValidationLabelColor");
                }
            }
        public string accountValidationLabel
            {
            get { return _accountValidationLabel; }
            set
                {
                _accountValidationLabel = value;
                OnPropertyChanged("accountValidationLabel");
                }
            }

        
        public string accountBoxValue
            {
            get { return _accountBoxValue; }
            set
                {
                _accountBoxValue = value;
                OnPropertyChanged("accountBoxValue");
                }
            }
        public string squestionBoxValue
            {
            get { return _squestionBoxValue; }
            set
                {
                _squestionBoxValue = value;
                OnPropertyChanged("squestionBoxValue");
                }
            }
        public string sanswerBoxValue
            {
            get { return _sanswerBoxValue; }
            set
                {
                _sanswerBoxValue = value;
                OnPropertyChanged("sanswerBoxValue");
                }
            }



       public string loginBoxValue
            {
            get { return _loginBoxValue; }
            set
                {
                _loginBoxValue = value;
                OnPropertyChanged("loginBoxValue");
                }
            }
        public string passwordBoxValue
            {
            get { return _passwordBoxValue; }
            set
                {
                _passwordBoxValue = value;
                OnPropertyChanged("passwordBoxValue");
                }
            }
        public string emailBoxValue
            {
            get { return _emailBoxValue; }
            set
                {
                _emailBoxValue = value;
                OnPropertyChanged("emailBoxValue");
                }
            }
        public string nameBoxValue
            {
            get { return _nameBoxValue; }
            set
                {
                _nameBoxValue = value;
                OnPropertyChanged("nameBoxValue");
                }
            }
        public string surnameBoxValue
            {
            get { return _surnameBoxValue; }
            set
                {
                _surnameBoxValue = value;
                OnPropertyChanged("surnameBoxValue");
                }
            }
        public string organizationBoxValue
            {
            get { return _organizationBoxValue; }
            set
                {
                _organizationBoxValue = value;
                OnPropertyChanged("organizationBoxValue");
                }
            }
        public string loginValidationLabel
            {
            get { return _loginValidationLabel; }
            set
                {
                _loginValidationLabel = value;
                OnPropertyChanged("loginValidationLabel");
                }
            }
        public string emailValidationLabel
            {
            get { return _emailValidationLabel; }
            set
                {
                _emailValidationLabel = value;
                OnPropertyChanged("emailValidationLabel");
                }
            }
        public string passwordValidationLabel
            {
            get { return _passwordValidationLabel; }
            set
                {
                _passwordValidationLabel = value;
                OnPropertyChanged("passwordValidationLabel");
                }
            }
        public string loginValidationLabelColor
            {
            get { return _loginValidationLabelColor; }
            set
                {
                _loginValidationLabelColor = value;
                OnPropertyChanged("loginValidationLabelColor");
                }
            }
        public string emailValidationLabelColor
            {
            get { return _emailValidationLabelColor; }
            set
                {
                _emailValidationLabelColor = value;
                OnPropertyChanged("emailValidationLabelColor");
                }
            }
        public string passwordValidationLabelColor
            {
            get { return _passwordValidationLabelColor; }
            set
                {
                _passwordValidationLabelColor = value;
                OnPropertyChanged("passwordValidationLabelColor");
                }
            }
        
        
        public bool enableRegisterButton
            {
            get { return _enableRegisterButton; }
            set
                {
                _enableRegisterButton = value;
                OnPropertyChanged("enableRegisterButton");
                }
            }

            
        #endregion

        #region Methods

        public void KillRegisterWindow()
        {
        WindowManager.Disapose(WindowName.Register); 
        }

        private void Login(object parameter)
            {
            var passwordContainer = parameter as IHavePassword;
            if (passwordContainer != null)
                {
                var secureString = passwordContainer.Password;
                passwordBoxValue = ConvertToUnsecureString(secureString);
                }
            PasswordTextBoxChangeMethod();
            }

        private string ConvertToUnsecureString(System.Security.SecureString securePassword)
            {
            if (securePassword == null)
                {
                return string.Empty;
                }

            IntPtr unmanagedString = IntPtr.Zero;
            try
                {
                unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString);
                }
            finally
                {
                System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
                }
            }

        public void ClickCancelMethod()
        {
        WindowManager.CloseWindow(WindowName.Register);
          
        }
        public void ClickRegisterMethod()
        {

         User user = new User(loginBoxValue, passwordBoxValue, emailBoxValue, nameBoxValue, surnameBoxValue, organizationBoxValue, accountBoxValue, squestionBoxValue, sanswerBoxValue);

         var result= usermanager.RegisterUser(user);
         if (result.errorsList.Count == 0)
         {

             MessageBox.Show("User successfully registered!");
                _regWindowLog.InfoFormat("User {0} succesfully registered", loginBoxValue);
                WindowManager.CloseWindow(WindowName.Register);
            }
            else
            {
                MessageBox.Show(result.errorsList[0].Message, "System error! New user not registered.", MessageBoxButton.OK, MessageBoxImage.Warning);
                _regWindowLog.Info("System error! New user not registered.");
                //WindowManager.CloseWindow(WindowName.Register);
            }

        }

        public void LoginTextBoxChangeMethod()
        {

        string loginPattern = @"^[a-zA-Z][a-zA-Z0-9-_]{2,10}$";

            if (loginBoxValue != null)
            {
                if (!Regex.IsMatch(loginBoxValue, loginPattern))
                {
                    loginValidationLabel = "No OK";
                    loginValidationLabelColor = "Red";
                    enableRegisterButton = false;
                }
                else
                {
                    loginValidationLabel = "OK";
                    loginValidationLabelColor = "Green";

                    if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                    {
                        enableRegisterButton = true;
                    }
                }
            }

        }

        public void EmailTextBoxChangeMethod()
        {

            string emailPattern = @"^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$";

            if (emailBoxValue != null)
            {
                if (!Regex.IsMatch(emailBoxValue, emailPattern))
                {
                    emailValidationLabel = "No OK";
                    emailValidationLabelColor = "Red";
                    enableRegisterButton = false;
                    
                }
                else
                {
                    emailValidationLabel = "OK";
                    emailValidationLabelColor = "Green";
                    if (loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                    {
                        enableRegisterButton = true;
                    }
                }
            }
        }

        public void PasswordTextBoxChangeMethod()
        {
        passwordValidationLabel = RegistrationWindowManager.PasswordStrengthChecker(passwordBoxValue);
        passwordValidationLabelColor = RegistrationWindowManager.PasswordValidationLabelColor(passwordValidationLabel);

        if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                {
                enableRegisterButton = true;
                }
            else
                {
                enableRegisterButton = false;
                }

            }

        public void NameTextBoxChangeMethod()
            {
            if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                {
                enableRegisterButton = true;
                }
            else
                {
                enableRegisterButton = false;
                }

            }
        public void SurnameTextBoxChangeMethod()
            {
            if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(surnameBoxValue) && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue))
                {
                enableRegisterButton = true;
                }
            else
                {
                enableRegisterButton = false;
                }

            }
        public void OrganizationTextBoxChangeMethod()
            {
            if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                {
                enableRegisterButton = true; 
                }
            else
            {
            enableRegisterButton = false; 
            }


            }
        public void AccountTextBoxChangeMethod()
            {
           

            string accountPattern = @"[0-9]{8}";

            if (accountBoxValue != null)
                {
                if (!Regex.IsMatch(accountBoxValue, accountPattern))
                    {
                    accountValidationLabel = "No OK";
                    accountValidationLabelColor = "Red";
                    enableRegisterButton = false;

                    }
                else
                    {
                    accountValidationLabel = "OK";
                    accountValidationLabelColor = "Green";
                    if (emailValidationLabel == "OK" && accountValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                        {
                        enableRegisterButton = true;
                        }
                    }
                }
            }
       
        public void SquestionTextBoxChangeMethod()
            {
            if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(squestionBoxValue) && !String.IsNullOrWhiteSpace(sanswerBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                {
                enableRegisterButton = true;
                }
            else
                {
                enableRegisterButton = false;
                }


            }
        public void SanswerTextBoxChangeMethod()
            {
            if (emailValidationLabel == "OK" && !String.IsNullOrWhiteSpace(sanswerBoxValue) && !String.IsNullOrWhiteSpace(squestionBoxValue) && accountValidationLabel == "OK" && loginValidationLabel == "OK" && !String.IsNullOrWhiteSpace(nameBoxValue) && !String.IsNullOrWhiteSpace(passwordBoxValue) && !String.IsNullOrWhiteSpace(surnameBoxValue))
                {
                enableRegisterButton = true;
                }
            else
                {
                enableRegisterButton = false;
                }


            }

            
            
        #endregion
        }
    }
