﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
    {
    class BalanceViewModel : BaseViewModel
    {

        private string _amountTextBoxText, _userTextBoxText, _amountBackgroundColor;
        private bool _enableAddButton;
        private User userBAL;


        public BalanceViewModel(User user)
        {
            userTextBoxText = user.Login;
            userBAL = user;
            radio10Command = new Command(arg => Radio10Method());
            radio100Command = new Command(arg => Radio100Method());
            radio1000Command = new Command(arg => Radio1000Method());
            amountTextChangeCommand = new Command(arg => AmountTextChangeMethod());
            addButtonClickCommand = new Command(arg => AddButtonClickMethod());
            closingWindowCommand = new Command(arg => ClosingWindowMethod());

        }
        #region Properties

        public ICommand radio10Command { get; set; }
        public ICommand radio100Command { get; set; }
        public ICommand radio1000Command { get; set; }
        public ICommand amountTextChangeCommand { get; set; }
        public ICommand addButtonClickCommand { get; set; }
        public ICommand closingWindowCommand { get; set; }



        public bool enableAddButton
        {
            get { return _enableAddButton; }
            set
            {
                _enableAddButton = value;
                OnPropertyChanged("enableAddButton");
            }
        }

        public string amountTextBoxText
            {
            get { return _amountTextBoxText; }
            set
                {
                _amountTextBoxText = value;
                OnPropertyChanged("amountTextBoxText");
                }
            }
        public string amountBackgroundColor
            {
            get { return _amountBackgroundColor; }
            set
                {
                _amountBackgroundColor = value;
                OnPropertyChanged("amountBackgroundColor");
                }
            }

        public string userTextBoxText
            {
            get { return _userTextBoxText; }
            set
                {
                _userTextBoxText = value;
                OnPropertyChanged("userTextBoxText");
                }
            }
    
        #endregion

        public void Radio10Method()
            {

            amountTextBoxText = "10";

            }
        public void Radio100Method()
            {

            amountTextBoxText = "100";

            }
        public void Radio1000Method()
            {

            amountTextBoxText = "1000";

            }

        public void AmountTextChangeMethod()
        {

        string amountPattern = @"^[0-9]+$";

            if (!Regex.IsMatch(amountTextBoxText, amountPattern))
            {
                amountBackgroundColor = "Pink";
                enableAddButton = false;
            }
            else
            {
                amountBackgroundColor = "White";
                enableAddButton = true;
            }

        }

        public void AddButtonClickMethod()

        {
            decimal ubalance;
            ubalance = Convert.ToDecimal(amountTextBoxText) + userBAL.Balance;
            usermanager.User.Balance = ubalance;
        var result = usermanager.UpdateBalance(userTextBoxText, ubalance);

            if (result != -1)
            {
           
                WindowManager.CloseWindow(WindowName.Balance);
                WindowManager.Disapose(WindowName.Balance);
            }
            else
            {
                MessageBox.Show("Balance not changed");
            }

        }

        public void ClosingWindowMethod()
        {
        WindowManager.Disapose(WindowName.Balance);   
        }
 }
       
    }
