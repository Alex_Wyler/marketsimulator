﻿using MarketSimulator.UI.Helpers;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using log4net;
using System.Text.RegularExpressions;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.ViewModel
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly ILog log = LogManager.GetLogger("[LoginViewModel] : ");
        private string _loginTextBoxValue, _passwordTextBoxValue, _loginBoxLabel;
        private bool _enableClickButton, _enableLoginTextBox, _enablePasswordBox, _anonymousLoginValue;

        public LoginViewModel()
        {
            logInButtonClickCommand = new Command(arg => ClickLoginButtonMethod());
            registrationLabelClickCommand = new Command(arg => RegistrationLabelClick());
            forgotPassLabelClickCommand = new Command(arg => ForgotPassLabelClick());
            loginWindowStartCommand = new Command(arg => LoginWindowStart());
            loginTextChangeCommand = new Command(arg => LoginTextBoxChangeMethod());
            passwordExtractCommand = new RelayCommand(PasswordExtract);
            checkBoxCheckedCommand = new Command(arg => CheckCommand());
            checkBoxUncheckedCommand = new Command(arg => UncheckCommand());

            log.Info("Login window opened");
            enablePasswordBox = false;
        }

        #region Properties

        public ICommand 
            logInButtonClickCommand { get; set; }
        public ICommand registrationLabelClickCommand { get; set; }
        public ICommand forgotPassLabelClickCommand { get; set; }
        public ICommand loginWindowStartCommand { get; set; }
        public ICommand loginTextChangeCommand { get; set; }
        public ICommand checkBoxCheckedCommand { get; set; }
        public ICommand checkBoxUncheckedCommand { get; set; }

        public ICommand passwordExtractCommand
            {
            get;
            private set;
            }


        public bool AnonymousLoginValue
        {
            get { return _anonymousLoginValue;}
            set
            {
                _anonymousLoginValue = value;
                OnPropertyChanged("AnonymousLoginValue");
            }
        }

        public string loginBoxLabel
            {
            get { return _loginBoxLabel; }
            set
                {
                _loginBoxLabel = value;
                OnPropertyChanged("loginBoxLabel");
                }
            }
        public bool enableLoginTextBox
            {
            get { return _enableLoginTextBox; }
            set
                {
                _enableLoginTextBox = value;
                OnPropertyChanged("enableLoginTextBox");
                }
            }
        public bool enablePasswordBox
            {
            get { return _enablePasswordBox; }
            set
                {
                _enablePasswordBox = value;
                OnPropertyChanged("enablePasswordBox");
                }
            }



        public bool enableClickButton
            {
            get { return _enableClickButton; }
            set
                {
                _enableClickButton = value;
                OnPropertyChanged("enableClickButton");
                }
            }

        public string LoginTextBoxValue
        {
            get { return _loginTextBoxValue; }
            set
            {
                _loginTextBoxValue = value;
                OnPropertyChanged("LoginTextBoxValue");
            }
        }

        public string PasswordTextBoxValue
        {
            get { return _passwordTextBoxValue; }
            set
            {
                _passwordTextBoxValue = value;
                OnPropertyChanged("PasswordTextBoxValue");
            }
        }

        #endregion Properties

        #region Methods

        public void CheckCommand()
        {
            enableClickButton = true;
            enableLoginTextBox = false;
            enablePasswordBox = false;


        }
        public void UncheckCommand()
            {
            enableClickButton = false;
            enableLoginTextBox = true;
            enablePasswordBox = true;


            }
        
        private void PasswordExtract(object parameter)
            {
            var passwordContainer = parameter as IHavePassword;
            if (passwordContainer != null)
                {
                var secureString = passwordContainer.Password;
                PasswordTextBoxValue = ConvertToUnsecureString(secureString);
                }
            if (!String.IsNullOrWhiteSpace(PasswordTextBoxValue) && !String.IsNullOrWhiteSpace(LoginTextBoxValue))
                {
                enableClickButton = true;
                }
            else enableClickButton = false;
           }
        
        private string ConvertToUnsecureString(System.Security.SecureString securePassword)
            {
            
            if (securePassword == null)
                {
                return string.Empty;
                }

            IntPtr unmanagedString = IntPtr.Zero;
            try
                {
                unmanagedString = System.Runtime.InteropServices.Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return System.Runtime.InteropServices.Marshal.PtrToStringUni(unmanagedString);
                }
            finally
                {
                System.Runtime.InteropServices.Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
                }
            }

        public static bool CheckLoginButton(string login,string pass)
        {
            if (!String.IsNullOrWhiteSpace(pass) && !String.IsNullOrWhiteSpace(login))
            {
                return true;
            }
            else 
                return false;
        }
        public void LoginTextBoxChangeMethod()
            {
              
               if (CheckEmailTextBox(LoginTextBoxValue))
               {
                   //loginBoxLabel = "E-mail";
                   //loginBoxLabel = "Login";
                 
                   enablePasswordBox = true;
                   enableClickButton = CheckLoginButton(LoginTextBoxValue,PasswordTextBoxValue);
               }
                if (CheckLoginTextBox(LoginTextBoxValue))
               {
                   //loginBoxLabel = "Login";
                    //loginBoxLabel = "Login";
                
                   enablePasswordBox = true;
                   enableClickButton = CheckLoginButton(LoginTextBoxValue, PasswordTextBoxValue);
               }
               if (String.IsNullOrWhiteSpace(LoginTextBoxValue))
               {
                   //LoginBoxLabel = "Login/E-mail";
                   //loginBoxLabel = "Login";
                   enablePasswordBox = false;
                   enableClickButton = false;

               }
               //else if (!String.IsNullOrWhiteSpace(LoginTextBoxValue))
               //    {
                   
                   
               //    enableClickButton = true;

               //    }
               //else
                   //enablePasswordBox = false;
               //enableClickButton = false;
            }


        public static bool CheckLoginTextBox(string logintextboxvalue)
        {

            string loginPattern = @"^[a-zA-Z][a-zA-Z0-9-_]{2,10}$";

            if (logintextboxvalue != null && Regex.IsMatch(logintextboxvalue, loginPattern))
            {
                return true;

            }
                return false;

        }

        public static bool CheckEmailTextBox(string emailtextboxvalue)
        {

            string emailPattern = @"^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$";

            if (emailtextboxvalue != null && Regex.IsMatch(emailtextboxvalue, emailPattern))
            {
                return true;

            }
                return false;
        }

        public void ClickLoginButtonMethod()
        {
            if (AnonymousLoginValue)
            {
                usermanager.User = null;
                WindowManager.OpenWindow(WindowName.Loading, WindowShowType.Show,
                        new LoadingViewModel() { usermanager = usermanager });
                WindowManager.CloseWindow(WindowName.Login);
                WindowManager.Disapose(WindowName.Login);
            }
            else
            {
                if (!enableLoginTextBox || !enablePasswordBox) return;
                if (!CheckLoginTextBox(LoginTextBoxValue) && !CheckEmailTextBox(LoginTextBoxValue)) return;

                var result = usermanager.Login(LoginTextBoxValue, PasswordTextBoxValue);
               
                if (result.errorsList.Count == 0)
                {
                    WindowManager.OpenWindow(WindowName.Loading, WindowShowType.Show,
                    new LoadingViewModel() { usermanager = usermanager });
                    WindowManager.CloseWindow(WindowName.Login);
                    WindowManager.Disapose(WindowName.Login);
                }
                else
                {
                    MessageBox.Show(result.errorsList[0].Message);
                }
            }
            
        }

        public void RegistrationLabelClick()
        {
            WindowManager.OpenWindow(WindowName.Register, WindowShowType.ShowDialog, new RegisterViewModel(){usermanager = this.usermanager});
        }

        public void ForgotPassLabelClick()
        {
            WindowManager.OpenWindow(WindowName.ForgotPassword, WindowShowType.ShowDialog, new ForgotPasswordViewModel() { usermanager=this.usermanager});
        }

        public void LoginWindowStart()
        {
            //loginBoxLabel = "Login/E-mail";
            loginBoxLabel = "Login";
            enableLoginTextBox = true;
            enablePasswordBox = false;
        }

        #endregion Methods
    }
}