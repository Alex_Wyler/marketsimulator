﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.View.Frames;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.ViewModel
    {
        public class BuyViewModel : BaseViewModel
        {
            public ICommand onWindowCloseCommand { get; set; }
            public ICommand yesButtonClickCommand { get; set; }
            public Item currentBuyItem { get; set; }
            private UserManager buyUserManager;
            private MarketFrameViewModel.UpdateListDelegate updateList;
            private StartupFrameViewModel.UpdateStartUpListDelegate updateStartList;

            private string _headerlabel, _ownerLabel, _priceLabel, _dateLabel;
            private BitmapImage _imageView;


            public BitmapImage Image
                {
                get { return _imageView; }
                set
                    {
                    _imageView = value;
                    OnPropertyChanged("Image");
                    }
                }


            public string dateLabel
            {
                get { return _dateLabel; }
                set
                {
                    _dateLabel = value;
                    OnPropertyChanged("dateLabel");
                }
            }

            public string priceLabel
            {
                get { return _priceLabel; }
                set
                {
                    _priceLabel = value;
                    OnPropertyChanged("priceLabel");
                }
            }



            public string headerLabel
            {
                get { return _headerlabel; }
                set
                {
                    _headerlabel = value;
                    OnPropertyChanged("headerLabel");
                }
            }

            public string ownerLabel
            {
                get { return _ownerLabel; }
                set
                {
                    _ownerLabel = value;
                    OnPropertyChanged("ownerLabel");
                }
            }

            public BuyViewModel(Item item, UserManager usermanager, MarketFrameViewModel.UpdateListDelegate updateListDelegate)
            {
                buyUserManager = usermanager;
                currentBuyItem = item;
                Image = new BitmapImage();
                Image = BitmapImageConverter.ConvertToBitMapImageFromByte(currentBuyItem.Photo);
                onWindowCloseCommand = new Command(arg => WindowCloseMethod());
                yesButtonClickCommand = new Command(arg => YesButtonClickMethod());
                headerLabel = currentBuyItem.Header;
                ownerLabel = currentBuyItem.Owner.Login;
                priceLabel = currentBuyItem.Price.ToString("0.##");
                dateLabel = currentBuyItem.YearMade.ToString("Y");
                updateList = updateListDelegate;
                
            }
            public BuyViewModel(Item item, UserManager usermanager, StartupFrameViewModel.UpdateStartUpListDelegate updateStartUpListDelegate)
                {
                buyUserManager = usermanager;
                currentBuyItem = item;
                Image = new BitmapImage();
                Image = BitmapImageConverter.ConvertToBitMapImageFromByte(currentBuyItem.Photo);
                onWindowCloseCommand = new Command(arg => WindowCloseMethod());
                yesButtonClickCommand = new Command(arg => YesButtonClickMethod());
                headerLabel = currentBuyItem.Header;
                ownerLabel = currentBuyItem.Owner.Login;
                priceLabel = currentBuyItem.Price.ToString("0.##");
                dateLabel = currentBuyItem.YearMade.ToString("Y");
                updateStartList = updateStartUpListDelegate;
                }

            public void WindowCloseMethod()
            {
                WindowManager.Disapose(WindowName.Buy);
                WindowManager.CloseWindow(WindowName.Buy);
            }

            public void YesButtonClickMethod()
            {
             
                if (buyUserManager.User.Balance > currentBuyItem.Price)
                {
                buyUserManager.User.Balance -= currentBuyItem.Price;

                var resultbuy = buyUserManager.UpdateBalance(buyUserManager.User.Login, buyUserManager.User.Balance);
                currentBuyItem.Owner.Balance += currentBuyItem.Price;
                var resultsell = buyUserManager.UpdateBalance(currentBuyItem.Owner.Login, currentBuyItem.Owner.Balance);
                ItemManager itm = new ItemManager();
                var resultstatus = itm.UpdateStatus(currentBuyItem.Id, false);

                    if (resultsell != -1 && resultbuy != -1 && resultstatus != -1)
                    {
                        MessageBox.Show("Transaction complete");

                        //MarketFrameViewModel mfvm = new MarketFrameViewModel();
                        //mfvm.GetAllItems();

                        //updateList(currentBuyItem.Id);
                        if (updateList == null) updateStartList(currentBuyItem.Id);
                        if (updateStartList == null) updateList(currentBuyItem.Id);
                        

                        WindowManager.CloseWindow(WindowName.Buy);
                        WindowManager.Disapose(WindowName.Buy);
                        //MarketFrame mfm = new MarketFrame();
                        //mfm.InitializeComponent();
                        //WindowManager.MainVM.MarketRunMethod();
                        
                    }
                    
                    else
                    {

                    MessageBox.Show("Transaction failed");
                    WindowManager.CloseWindow(WindowName.Buy);  
                    WindowManager.Disapose(WindowName.Buy);
                    
                    }

                }

                else
                {
                    MessageBox.Show("Insuficient funds");
                    WindowManager.CloseWindow(WindowName.Buy);
                    WindowManager.Disapose(WindowName.Buy);
                    

                }

            }
        }
    }
