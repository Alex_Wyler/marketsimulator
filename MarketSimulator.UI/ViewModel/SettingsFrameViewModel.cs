﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Threading.Tasks;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
{
    public class SettingsFrameViewModel : BaseViewModel
    {
        public UserAdress CurrentAdress { get; set; }
        public UserAdress Adress;
        public SettingsFrameViewModel()
        {
            ApplyCommand = new Command(arg => Apply());
        }

        public void Apply()
        {
            Adress.StreetName = CurrentAdress.StreetName;
            Adress.HouseNumber = CurrentAdress.HouseNumber;
            Adress.HouseCharacter = CurrentAdress.HouseCharacter;
            Adress.PostalCode = CurrentAdress.PostalCode;
            Adress.City = CurrentAdress.City;
            Adress.Municipality = CurrentAdress.Municipality;
            Adress.Province = CurrentAdress.Province;
            Adress.Country = CurrentAdress.Country;
        }

        public ICommand ApplyCommand;
    }
}
