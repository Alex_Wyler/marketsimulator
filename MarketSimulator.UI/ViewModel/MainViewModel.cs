﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.View;
using MarketSimulator.UI.View.Frames;
using MarketSimulator.UI.ViewModel.FrameViewModel;


namespace MarketSimulator.UI.ViewModel
    {
        public class MainViewModel : BaseViewModel
    {
        private string _displayFrame, _loggedUserName, _logButtonContent, _balanceButtonContent;
        public LinkedList<string> frameHistory = new LinkedList<string>();
        public LinkedList<BaseViewModel> vmHistory = new LinkedList<BaseViewModel>();
        private bool _enableBackButton, _enableLogButton=true, _enableBalanceButton, _enableAddItemButton, _enableProfileButton, _enableReturnButton;
        private BaseViewModel _frameDataContext;
        public List<Item> ItemsList;
       

        
       
        
        public MainViewModel()

            {
            exitButtonClickCommand = new Command(arg => ExitMethod());
            minimizeButtonClickCommand = new Command(arg => MinimizeMethod());
            profileButtonClickCommand = new Command(arg => ProfileRunMethod());
            settingsButtonClickCommand = new Command(arg => SettingsRunMethod());
            balanceButtonClickCommand = new Command(arg => BalanceRunMethod());
            marketButtonClickCommand = new Command(arg => MarketRunMethod());
            backButtonClickCommand = new Command(arg => BackButtonMethod());
            windowLoadedCommand = new Command(arg => WindowLoadedMethod());
            windowActivatedCommand = new Command(arg => WindowActivatedMethod());
            addItemButtonClickCommand = new Command(arg => OpenAddItemWindowMethod());
            returnItemsButtonClickCommand = new Command(arg => ReturnItemsMethod());
            logoutButtonClickCommand = new Command(arg => LogoutMethod());
            }

            #region Properties
            public ICommand exitButtonClickCommand { get; set; }
            public ICommand minimizeButtonClickCommand { get; set; }
            public ICommand profileButtonClickCommand { get; set; }
            public ICommand settingsButtonClickCommand { get; set; }
            public ICommand balanceButtonClickCommand { get; set; }
            public ICommand marketButtonClickCommand { get; set; }
            public ICommand backButtonClickCommand { get; set; }
            public ICommand windowLoadedCommand { get; set; }
            public ICommand windowActivatedCommand { get; set; }
            public ICommand addItemButtonClickCommand { get; set; }
            public ICommand returnItemsButtonClickCommand { get; set; }
            public ICommand logoutButtonClickCommand { get; set; }

            

        public BaseViewModel FrameDataContext
        {
        get { return _frameDataContext; }
            set
            {
                _frameDataContext = value;
                OnPropertyChanged("FrameDataContext");
            }
        }
        public string logButtonContent
            {
            get { return _logButtonContent; }
            set
                {
                _logButtonContent = value;
                OnPropertyChanged("logButtonContent");
                }
            }

        public string balanceButtonContent
            {
            get { return _balanceButtonContent; }
            set
                {
                _balanceButtonContent = value;
                OnPropertyChanged("balanceButtonContent");
                }
            }
        public string loggedUserName
                {
                get { return _loggedUserName; }
                set
                    {
                    _loggedUserName = value;
                    OnPropertyChanged("loggedUserName");
                    }
                }
            public string displayFrame
                {
                get { return _displayFrame; }
                set
                    {
                    _displayFrame = value;
                    OnPropertyChanged("displayFrame");
                    }
                }
            public bool enableBackButton
                {
                get { return _enableBackButton; }
                set
                    {
                    _enableBackButton = value;
                    OnPropertyChanged("enableBackButton");
                    }
                }

            public bool enableBalanceButton
                {
                get { return _enableBalanceButton; }
                set
                    {
                    _enableBalanceButton = value;
                    OnPropertyChanged("enableBalanceButton");
                    }
                }
            public bool enableLogButton
                {
                get { return _enableLogButton; }
                set
                    {
                    _enableLogButton = value;
                    OnPropertyChanged("enableLogButton");
                    }
                }

            public bool enableAddItemButton
            {
                get { return _enableAddItemButton;}
                set
                {
                    _enableAddItemButton = value;
                    OnPropertyChanged("enableAddItemButton");
                }
            }
            public bool enableProfileButton
                {
                get { return _enableProfileButton; }
                set
                    {
                    _enableProfileButton = value;
                    OnPropertyChanged("enableProfileButton");
                    }
                }

            public bool enableReturnButton
                {
                get { return _enableReturnButton; }
                set
                    {
                    _enableReturnButton = value;
                    OnPropertyChanged("enableReturnButton");
                    }
                }
        
            #endregion
            
            
            #region Methods
            public void ExitMethod()
              {
            
                WindowManager.CloseWindow(WindowName.Main);
               
               }
            public void MinimizeMethod()
                {
                
                WindowManager.MinimizeWindow(WindowName.Main);
                }



            public void ProfileRunMethod()
            {
                //OpenFrame(FrameName.Profile);
            WindowManager.OpenWindow(WindowName.ProfileInfo, WindowShowType.ShowDialog, new ProfileInfoViewModel(usermanager.User){ usermanager = usermanager });

                
            }
            public void SettingsRunMethod()
                {
                WindowManager.OpenWindow(WindowName.Settings, WindowShowType.ShowDialog, new SettingsWindowViewModel(){usermanager = usermanager});
               
                }
            public void BalanceRunMethod()
                {
                WindowManager.OpenWindow(WindowName.Balance, WindowShowType.ShowDialog, new BalanceViewModel(usermanager.User){usermanager = usermanager});
                
                }
            public void MarketRunMethod()
                {
                OpenFrame(FrameName.Market);
                    
                }

            public void OpenAddItemWindowMethod()
            {
                //WindowManager.OpenWindow(WindowName.AddItem, WindowShowType.ShowDialog, new AddItemViewModel(){usermanager = usermanager});
            }

        private void OpenFrame(FrameName name)
        {
            switch (name)
            {
                    case FrameName.Startup:
                    enableReturnButton = false;
                    displayFrame = "Frames/StartupFrame.xaml";
                    FrameDataContext = new StartupFrameViewModel(usermanager);
                    
                    break;
                    
                    case FrameName.Market:
                    //FrameDataContext = null;
                    //displayFrame = "Frames/CategoriesFrame.xaml";
                    enableReturnButton = true;
                    displayFrame = "Frames/MarketFrame.xaml";
                    FrameDataContext = new MarketFrameViewModel() { usermanager = usermanager };
                    
                    break;
                    
                    //case FrameName.Profile:

                    //displayFrame = "Frames/ProfileFrame.xaml";
                    //FrameDataContext = new ProfileFrameViewModel() { usermanager = usermanager };

                    //break;

            }
            frameHistory.AddLast(displayFrame);
            vmHistory.AddLast(FrameDataContext);

            if (frameHistory.Count != 1)
            {
                enableBackButton = true;
            }

                
        }

            public void BackButtonMethod()
            {
                
                if (frameHistory.Count > 1)
                {
                    frameHistory.RemoveLast();
                    vmHistory.RemoveLast();
                    displayFrame = frameHistory.Last();
                    FrameDataContext =  vmHistory.Last();
                    if (displayFrame == "Frames/StartupFrame.xaml") enableReturnButton = false;
                    if (displayFrame == "Frames/MarketFrame.xaml") enableReturnButton = true;

                }

                if (frameHistory.Count == 1) enableBackButton = false;
               
                

            }
            public void WindowLoadedMethod()
            {
            //ItemManager itm = new ItemManager();
            //var resultstatus = itm.UpdateStatus(1, true);

                if (usermanager.User != null)
                {
                    loggedUserName = "User: " + usermanager.User.Login;
                    logButtonContent = "Logout";
                    enableBalanceButton = true;
                    enableAddItemButton = true;
                    enableProfileButton = true;
                    balanceButtonContent = usermanager.User.Balance.ToString("N");
                    
                }
                else
                { 
                loggedUserName = "User: Anonymous";
                    logButtonContent = "Login";
                    enableBalanceButton = false;
                    enableAddItemButton = false;
                    enableProfileButton = false;
                    balanceButtonContent = "Balance";

                }
                OpenFrame(FrameName.Startup);
              
            }

            public void WindowActivatedMethod()
            {
                if (usermanager.User != null)
                {
                    balanceButtonContent = usermanager.User.Balance.ToString("N");
                }
                //MarketFrameViewModel mfvm = new MarketFrameViewModel();
                //mfvm.GetAllItems();
            }

            public void LogoutMethod()
            {

            usermanager = null;
            WindowManager.OpenWindow(WindowName.Login, WindowShowType.Show, new LoginViewModel() { usermanager = new UserManager() { RegisterManager = new RegisterManager(), ItemManager = new ItemManager() } });
            WindowManager.CloseWindow(WindowName.Main);
               

                


            }

            public void ReturnItemsMethod()
            {
                var itm = new ItemManager();

                if (itm.ReturnStatus() != -1)
                {
                    MessageBox.Show("All items returned to database.");
                }
                //if (itm.ReturnStatus() == 7)
                //    {
                //    MessageBox.Show("All items already in database. No operations performed");
                //    }

                else
                {
                    MessageBox.Show("Error while return items to database!");
                }
                
            }
                
               


            #endregion
        }
 
    }
