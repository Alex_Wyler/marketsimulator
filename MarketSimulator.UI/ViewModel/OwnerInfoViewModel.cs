﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
    {
    class OwnerInfoViewModel : BaseViewModel

    {
        private string _emailBoxValue, _nameBoxValue, _surnameBoxValue, _organizationBoxValue, _ownerLabelContent;
        
        public ICommand onWindowCloseCommand { get; set; }


        public OwnerInfoViewModel(Item item)
        {
        onWindowCloseCommand = new Command(arg => WindowCloseMethod());
            emailBoxValue = item.Owner.Email;
            nameBoxValue = item.Owner.FirstName;
            surnameBoxValue = item.Owner.LastName;
            organizationBoxValue = item.Owner.CompanyName;
            ownerLabelContent = "Owner of " + item.Header + " info";
        }

        public void WindowCloseMethod()
            {
            WindowManager.Disapose(WindowName.OwnerInfo);
            WindowManager.CloseWindow(WindowName.OwnerInfo);
            
            }
        public string ownerLabelContent
            {
            get { return _ownerLabelContent; }
            set
                {
                _ownerLabelContent = value;
                OnPropertyChanged("ownerLabelContent");
                }
            }
        public string emailBoxValue
            {
            get { return _emailBoxValue; }
            set
                {
                _emailBoxValue = value;
                OnPropertyChanged("emailBoxValue");
                }
            }
        public string nameBoxValue
            {
            get { return _nameBoxValue; }
            set
                {
                _nameBoxValue = value;
                OnPropertyChanged("nameBoxValue");
                }
            }
        public string surnameBoxValue
            {
            get { return _surnameBoxValue; }
            set
                {
                _surnameBoxValue = value;
                OnPropertyChanged("surnameBoxValue");
                }
            }
        public string organizationBoxValue
            {
            get { return _organizationBoxValue; }
            set
                {
                _organizationBoxValue = value;
                OnPropertyChanged("organizationBoxValue");
                }
            }
        }
    }
