﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MarketSimulator.BL;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
{
    public class LoadingViewModel : BaseViewModel
    {
        public ICommand LoadWindowCommand { get; set; }

        public LoadingViewModel()
        {
            LoadWindowCommand = new Command(arg => LoadingMethod());
        }

        public void LoadingMethod()
        {
            LoadAsync();
        }

        private async void LoadAsync()
        {
            var result = await LoadingAsync();
            if (result)
            {
                OpenMainPage();
            }
        }

        private Task<bool> LoadingAsync()
        {
            const bool result = true;

            return Task.Run(() =>
            {
                usermanager.ItemManager.GetAllItems();
               

                return result;
            });
        }

        private void OpenMainPage()
        {
            WindowManager.OpenWindow(WindowName.Main, WindowShowType.Show,
                   new MainViewModel() { usermanager = usermanager });
            WindowManager.CloseWindow(WindowName.Loading);
            WindowManager.Disapose(WindowName.Loading);
        }
    }
}