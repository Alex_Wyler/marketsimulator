﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
    {
    class ProfileInfoViewModel : BaseViewModel
        {
        private string _emailBoxValue, _nameBoxValue, _surnameBoxValue, _organizationBoxValue, _userLabelContent, _bankAccountBoxValue, _balanceBoxValue;
        
        public ICommand onWindowCloseCommand { get; set; }


        public ProfileInfoViewModel(User user)
        {
        onWindowCloseCommand = new Command(arg => WindowCloseMethod());
        emailBoxValue = user.Email;
        nameBoxValue = user.FirstName;
        surnameBoxValue = user.LastName;
        organizationBoxValue = user.CompanyName;
            userLabelContent = user.Login + " info";
            bankAccountBoxValue = user.BankAccount;
            balanceBoxValue = user.Balance.ToString("N");

        }

        public void WindowCloseMethod()
            {
            WindowManager.Disapose(WindowName.ProfileInfo);
            WindowManager.CloseWindow(WindowName.ProfileInfo);
            
            }
        public string balanceBoxValue
            {
            get { return _balanceBoxValue; }
            set
                {
                _balanceBoxValue = value;
                OnPropertyChanged("balanceBoxValue");
                }
            }
        public string bankAccountBoxValue
            {
            get { return _bankAccountBoxValue; }
            set
                {
                _bankAccountBoxValue = value;
                OnPropertyChanged("bankAccountBoxValue");
                }
            }
        public string userLabelContent
            {
            get { return _userLabelContent; }
            set
                {
                _userLabelContent = value;
                OnPropertyChanged("userLabelContent");
                }
            }
        public string emailBoxValue
            {
            get { return _emailBoxValue; }
            set
                {
                _emailBoxValue = value;
                OnPropertyChanged("emailBoxValue");
                }
            }
        public string nameBoxValue
            {
            get { return _nameBoxValue; }
            set
                {
                _nameBoxValue = value;
                OnPropertyChanged("nameBoxValue");
                }
            }
        public string surnameBoxValue
            {
            get { return _surnameBoxValue; }
            set
                {
                _surnameBoxValue = value;
                OnPropertyChanged("surnameBoxValue");
                }
            }
        public string organizationBoxValue
            {
            get { return _organizationBoxValue; }
            set
                {
                _organizationBoxValue = value;
                OnPropertyChanged("organizationBoxValue");
                }
            }
        }
    }
