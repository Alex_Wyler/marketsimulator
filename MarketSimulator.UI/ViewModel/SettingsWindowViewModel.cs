﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.ViewModel
{
    class SettingsWindowViewModel: BaseViewModel
    {
        private string _oldPassword;
        private string _newPassword;
        private string _passwordValidationLabel;
        private string _passwordValidationLabelColor;
        private bool _isConfirmButtonEnable;
        private bool isFromRecovery;
        private bool oldPasswordIsEnabled;

        public bool IsConfirmButtonEnable
            {
            get { return _isConfirmButtonEnable; }
            set
                {
                _isConfirmButtonEnable = value;
                OnPropertyChanged("IsConfirmButtonEnable");
                }
            }
        public string PasswordValidationLabel
            {
            get { return _passwordValidationLabel; }
            set
                {
                _passwordValidationLabel = value;
                OnPropertyChanged("PasswordValidationLabel");
                }
            }

        public string PasswordValidationLabelColor
            {
            get { return _passwordValidationLabelColor; }
            set
                {
                _passwordValidationLabelColor = value;
                OnPropertyChanged("PasswordValidationLabelColor");
                }
            }
        

        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                _oldPassword = value;
                OnPropertyChanged("OldPassword");
            }
        }

        public string NewPassword
        {
            get { return _newPassword; }
            set
            {
                _newPassword = value;
                OnPropertyChanged("NewPassword");
            }
        }



        public bool OldPasswordIsEnabled
        {
            get { return oldPasswordIsEnabled; }
            set
            {
                oldPasswordIsEnabled = value;
                OnPropertyChanged("OldPasswordIsEnabled");
            }
        }


        public SettingsWindowViewModel()
        {
            ConfirmButtonClickCommand = new Command(arg => ConfirmButtonClick());
            CancelClickCommand = new Command(arg => CancelClick());
            NewPasswordTextChangeCommand = new Command(arg => NewPasswordValidationMethod());
            OldPasswordTextChangeCommand = new Command(arg => OldPasswordValidationMethod());
            OldPasswordIsEnabled = true;

        }

        public SettingsWindowViewModel(bool fromRecovery)
        {
            ConfirmButtonClickCommand = new Command(arg => ConfirmButtonClick());
            CancelClickCommand = new Command(arg => CancelClick());
            NewPasswordTextChangeCommand = new Command(arg => NewPasswordValidationMethod());
            OldPasswordTextChangeCommand = new Command(arg => OldPasswordValidationMethod());

            isFromRecovery = fromRecovery;

            if (isFromRecovery)
            {
                OldPassword = "******";
                OldPasswordIsEnabled = false;
            }

        }

        private void CancelClick()
        {
            try
            {
                WindowManager.CloseWindow(WindowName.Settings);
                WindowManager.Disapose(WindowName.Settings);         
            }
            catch (Exception)
            {
                
                WindowManager.Disapose(WindowName.Settings);
            }
        }

        private void ConfirmButtonClick()
        {
            if (isFromRecovery)
            {
                if (usermanager.User.Password != NewPassword)
                {
                    usermanager.ChangePassword(NewPassword);
                    WindowManager.OpenWindow(WindowName.Loading, WindowShowType.Show,
                    new LoadingViewModel() { usermanager = usermanager });
                    CancelClick();
                }
                else
                {
                    MessageBox.Show("New password can't be the same.");
                }
                
            }
            else
            {
                if (OldPassword != NewPassword)
                {
                    usermanager.ChangePassword(NewPassword);
                    CancelClick();
                }
                else
                {
                    MessageBox.Show("New password can't be the same.");
                }
            }

        }

        private void NewPasswordValidationMethod()
        {
            PasswordValidationLabel = RegistrationWindowManager.PasswordStrengthChecker(NewPassword);
            PasswordValidationLabelColor = RegistrationWindowManager.PasswordValidationLabelColor(PasswordValidationLabel);
            if (PasswordValidationLabel != "No OK" && OldPassword==usermanager.User.Password)
            {
                IsConfirmButtonEnable = true;
            }
            else
            {
                IsConfirmButtonEnable = false;
            }


            if (isFromRecovery)
            {
                IsConfirmButtonEnable = true;
            }
        }

        private void OldPasswordValidationMethod()
            {
            if (!String.IsNullOrWhiteSpace(NewPassword) && PasswordValidationLabel!= "No OK" && OldPassword == usermanager.User.Password)
                {
                IsConfirmButtonEnable = true;
                }
            else
                {
                IsConfirmButtonEnable = false;
                }
            
            }

        public ICommand ConfirmButtonClickCommand { get; set; }
        public ICommand CancelClickCommand { get; set; }
        public ICommand NewPasswordTextChangeCommand { get; set; }
        public ICommand OldPasswordTextChangeCommand { get; set; }

    }
}
