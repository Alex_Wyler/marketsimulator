﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xaml;
using MarketSimulator.BL;
using MarketSimulator.UI.View;
using MarketSimulator.UI.View.Frames;
using MarketSimulator.UI.ViewModel;
using MarketSimulator.UI.ViewModel.FrameViewModel;


namespace MarketSimulator.UI.Helpers
{
    public static class WindowManager
    {
        public static LoginWindow Login { get; set; }
        public static RegisterWindow Register { get; set; }
        public static BuyWindow Buy { get; set; }
        public static BalanceWindow Balance { get; set; }
        //public static ShowMoreView ShowMore { get; set; }
        public static ForgotPasswordWindow ForgotPassword { get; set; }
        //public static MainWindow Main { get; set; }
        public static View.MainWindow Main { get; set; }
        public static MainViewModel MainVM { get; set; }
        //public static UI.MainWindow DataContext { get; set; }
        public static AddItemWindow AddItem { get; set; }
        public static OwnerInfoWindow Owner { get; set; }
        public static ProfileInfoWindow Profile { get; set; }
        public static StartupFrame Startup { get; set; }
        public static SettingsWindow Settings { get; set; }
        public static LoadingWindow Loading { get; set; }


        #region Methods

        public static void OpenWindow(WindowName windowName, WindowShowType windowShowType, object viewmodel)
        {
            switch (windowName)
            {
                case WindowName.Login:
                    if (Login == null)
                    {
                        Login = new LoginWindow
                        {
                            DataContext = (LoginViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Login);
                    }
                    break;
                case WindowName.Register:
                    if (Register == null)
                    {
                        Register = new RegisterWindow
                        {
                            DataContext = (RegisterViewModel) viewmodel

                        };
                        ShowWindow(windowShowType, Register);
                    }
                    break;
                case WindowName.ForgotPassword:
                    if (ForgotPassword == null)
                    {
                        ForgotPassword = new ForgotPasswordWindow
                        {
                            DataContext = (ForgotPasswordViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, ForgotPassword);
                    }
                    break;
                case WindowName.Main:

                    if (Main == null)
                    {
                        Main = new View.MainWindow
                        {
                            DataContext = (MainViewModel) viewmodel
                        };
                        MainVM = (MainViewModel) viewmodel;
                        ShowWindow(windowShowType, Main);
                    }
                    break;
                case WindowName.AddItem:
                    if (AddItem == null)
                    {
                        AddItem = new AddItemWindow
                        {
                            DataContext = (AddItemViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, AddItem);
                    }
                    break;

                case WindowName.Buy:
                    if (Buy == null)
                    {
                        Buy = new BuyWindow
                        {
                            DataContext = (BuyViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Buy);
                    }
                    break;
                case WindowName.Balance:
                    if (Balance == null)
                    {
                        Balance = new BalanceWindow
                        {
                            DataContext = (BalanceViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Balance);
                    }
                    break;
                case WindowName.Settings:
                    if (Settings == null)
                    {
                        Settings = new SettingsWindow()
                        {
                            DataContext = (SettingsWindowViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Settings);
                    }
                    break;
                case WindowName.OwnerInfo:
                    if (Owner == null)
                    {
                        Owner = new OwnerInfoWindow
                        {
                            DataContext = (OwnerInfoViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Owner);
                    }
                    break;
                case WindowName.ProfileInfo:
                    if (Profile == null)
                    {
                        Profile = new ProfileInfoWindow
                        {
                            DataContext = (ProfileInfoViewModel) viewmodel
                        };
                        ShowWindow(windowShowType, Profile);
                    }
                    break;
                case WindowName.Loading:
                    if (Loading == null)
                    {
                        Loading = new LoadingWindow
                        {
                            DataContext = (LoadingViewModel)viewmodel
                        };
                        ShowWindow(windowShowType, Loading);
                    }
                    break;
            }
        }

        static void ShowWindow(WindowShowType windowShowType, Window window)
        {
            switch (windowShowType)
            {
                case WindowShowType.Show:
                    window.Show();
                    break;

                case WindowShowType.ShowDialog:
                    window.ShowDialog();
                    break;
            }
        }

        public static void CloseWindow(WindowName windowName)
        {
            switch (windowName)
            {
                case WindowName.Login:
                    if (Login != null)
                    {
                        Login.Close();
                        Login = null;
                    }
                    break;
                case WindowName.Register:
                    if (Register != null)
                    {
                        Register.Close();
                        Register = null;
                    }
                    break;
                case WindowName.ForgotPassword:
                    if (ForgotPassword != null)
                    {
                        ForgotPassword.Close();
                        ForgotPassword = null;
                    }
                    break;
                case WindowName.Main:
                    if (Main != null)
                    {
                        Main.Close();
                        Main = null;
                    }
                    break;
                case WindowName.AddItem:
                    if (AddItem != null)
                    {
                        AddItem.Close();
                    }
                    break;
                case WindowName.Buy:
                    if (Buy != null)
                    {
                        Buy.Close();
                    }
                    break;
                case WindowName.Balance:
                    if (Balance != null)
                    {
                        Balance.Close();
                    }
                    break;
                case WindowName.Settings:
                    if (Settings != null)
                    {
                        Settings.Close();
                    }
                    break;
                case WindowName.OwnerInfo:
                    if (Owner != null)
                    {
                        Owner.Close();
                    }
                    break;
                case WindowName.ProfileInfo:
                    if (Profile != null)
                    {
                        Profile.Close();
                    }
                    break;
                case WindowName.Loading:
                    if (Loading != null)
                    {
                        Loading.Close();
                    }
                    break;
            }
        }

        public static void Disapose(WindowName windowName)
        {
            switch (windowName)
            {
                case WindowName.Login:
                    Login = null;
                    break;
                case WindowName.Register:
                    Register = null;
                    break;
                case WindowName.ForgotPassword:
                    ForgotPassword = null;
                    break;
                case WindowName.Main:
                    Main = null;
                    break;
                case WindowName.AddItem:
                    AddItem = null;
                    break;
                case WindowName.Buy:
                    Buy = null;
                    break;
                case WindowName.Balance:
                    Balance = null;
                    break;
                case WindowName.Settings:
                    Settings = null;
                    break;
                case WindowName.OwnerInfo:
                    Owner = null;
                    break;
                case WindowName.ProfileInfo:
                    Profile = null;
                    break;
                case WindowName.Loading:
                    Loading = null;
                    break;
            }
        }

        public static void MinimizeWindow(WindowName windowName)
        {
            switch (windowName)
            {
                case WindowName.Login:
                    Login.WindowState = WindowState.Minimized;
                    break;

                case WindowName.Register:
                    Register.WindowState = WindowState.Minimized;
                    break;

                case WindowName.ForgotPassword:
                    ForgotPassword.WindowState = WindowState.Minimized;
                    break;

                case WindowName.Main:
                    Main.WindowState = WindowState.Minimized;
                    break;

                case WindowName.AddItem:
                    AddItem.WindowState = WindowState.Minimized;
                    break;
            }
        }

        //public static void OpenFrame(FrameName frameName, object viewmodel)
        //    {
        //    switch (frameName)
        //        {
        //        case FrameName.Startup:

        //            if (Startup == null)
        //            {

        //                Main.Frame.DataContext = new StartupFrameViewModel { usermanager = MainVM.usermanager };
        //                ShowWindow(windowShowType, Login);
        //                }
        //            break;

        //        case WindowName.Register:
        //            if (Register == null)
        //                {
        //                Register = new RegisterWindow
        //                {
        //                    DataContext = (RegisterViewModel)viewmodel

        //                };
        //                ShowWindow(windowShowType, Register);
        //                }
        //            break;
        //        case WindowName.ForgotPassword:
        //            if (ForgotPassword == null)
        //                {
        //                ForgotPassword = new ForgotPasswordWindow
        //                {
        //                    DataContext = (ForgotPasswordViewModel)viewmodel
        //                };
        //                ShowWindow(windowShowType, ForgotPassword);
        //                }
        //            break;
        //        case WindowName.Main:

        //            if (Main == null)
        //                {
        //                Main = new View.MainWindow
        //                {
        //                    DataContext = (MainViewModel)viewmodel
        //                };
        //                ShowWindow(windowShowType, Main);
        //                }
        //            break;
        //        }
        //    }

        #endregion
    }
}
