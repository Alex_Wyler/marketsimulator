﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MarketSimulator.UI.Helpers
{
    public class RelayCommand<T> : ICommand
        {
        private Func<object, object> _execute;

        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<T> execute)
            {
            }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(Object parameter)
            {
            _execute((T)parameter);
            }

        public event EventHandler CanExecuteChanged;
        }
}
       