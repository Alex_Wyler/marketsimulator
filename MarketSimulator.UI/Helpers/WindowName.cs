﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.UI.Helpers
{
    public enum WindowName
    {
        Login,
        Register,
        ForgotPassword,
        Main,
        AddItem,
        FirstEnterSettings,
        Buy,
        ShowMore,
        Settings,
        Balance,
        OwnerInfo,
        ProfileInfo,
        Loading
    }

    public enum FrameName
    {
        Startup,
        Profile,
        Balance,
        Market,
        Categories,
        Statictics,
        Settings
    
    }
}
