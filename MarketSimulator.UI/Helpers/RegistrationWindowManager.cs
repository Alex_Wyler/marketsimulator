﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarketSimulator.UI.Helpers
    {
    public static class RegistrationWindowManager
    
#region Methods

        {
        
        public static string PasswordValidationLabelColor(string passwordstrength)
        {
            string color = "";

            switch (passwordstrength)
            {
                case "poor":
                    color = "Yellow";
                    break;
                case "average":
                    color = "Blue";
                    break;
                case "strong":
                    color = "Green";
                    break;
                case "very strong":
                    color = "Green";
                    break;
                case "super strong":
                    color = "Green";
                    break;
                case "No OK":
                    color = "Red";
                    break;
            }
            return color;

        }

        public static string PasswordStrengthChecker(string password)
        {
        List<Regex> patterns = new List<Regex>{ new Regex(@"[a-z]"), new Regex(@"[0-9]"), new Regex(@"\W"), new Regex(@"[A-Z]")};
            int strength = 0, difficultyCoeff = 1;
            string passwordstrength = "";
            
            if (password.Length >= 6 && password.Length <= 7) { difficultyCoeff = 1; }
            if (password.Length >= 8 && password.Length <= 10) { difficultyCoeff = 2; }
            if (password.Length >= 11 && password.Length <= 12) { difficultyCoeff = 3; }

            if (password.Length >= 6)
            {
                foreach (var pattern in patterns)
                {
                    if (pattern.IsMatch(password))
                    {
                        strength++;
                    }
                }
            }
           
            if (password.Length < 6)
                {
                strength = 0;
                }

            strength = strength * difficultyCoeff;

            if (password.Length < 6)
            {
                strength = 0;
            }

                    switch (strength)
                        {
                        case 0:
                            passwordstrength = "No OK";
                            break;
                        case 1:
                            passwordstrength = "poor";
                            break;
                        case 2:
                            passwordstrength = "poor";
                            break;
                        case 3:
                            passwordstrength = "average";
                            break;
                        case 4:
                            passwordstrength = "average";
                            break;
                        case 6:
                            passwordstrength = "strong";
                            break;
                        case 8:
                            passwordstrength = "strong";
                            break;
                        case 9:
                            passwordstrength = "very strong";
                            break;
                        case 12:
                            passwordstrength = "very strong";
                            break;
                        
                        }
            return passwordstrength;
        }
        
        }

#endregion
    }
