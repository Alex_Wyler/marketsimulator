﻿namespace MarketSimulator.UI.Helpers
{
    public interface IHavePassword
        {
        System.Security.SecureString Password { get; }
        }
}