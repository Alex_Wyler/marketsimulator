﻿using System.Windows.Controls;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.View.Frames
    {
    /// <summary>
    /// Логика взаимодействия для StartupFrame.xaml
    /// </summary>
    public partial class StartupFrame : Page
        {
        public StartupFrame()
            {
            InitializeComponent();
            DataContext = new StartupFrameViewModel(WindowManager.MainVM.usermanager);
            }
        }
    }
