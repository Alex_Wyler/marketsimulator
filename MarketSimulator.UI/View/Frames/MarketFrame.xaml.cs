﻿using System.Windows.Controls;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel.FrameViewModel;

namespace MarketSimulator.UI.View.Frames
    {
    /// <summary>
    /// Логика взаимодействия для MarketFrame.xaml
    /// </summary>
    public partial class MarketFrame : Page
        {
        public MarketFrame()
            {
            InitializeComponent();
            DataContext = new MarketFrameViewModel() { usermanager = WindowManager.MainVM.usermanager };
            }
        }
    }
