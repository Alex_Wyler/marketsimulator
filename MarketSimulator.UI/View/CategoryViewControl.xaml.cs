﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel;

namespace MarketSimulator.UI.View
{
    /// <summary>
    /// Логика взаимодействия для CategoryViewControl.xaml
    /// </summary>
    public partial class CategoryViewControl : UserControl
    {
        public CategoryViewControl()
        {
            InitializeComponent();
            DataContext = new CategoryViewModelControl();
            {
              //  usermanager = WindowManager.MainVM.usermanager;
            };
        }
    }
}
