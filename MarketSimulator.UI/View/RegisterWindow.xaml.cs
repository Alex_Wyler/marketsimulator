﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MarketSimulator.UI.Helpers;
using MarketSimulator.UI.ViewModel;

namespace MarketSimulator.UI.View
    {
    /// <summary>
    /// Логика взаимодействия для RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window, IHavePassword
        {
        public RegisterWindow()
            {
            InitializeComponent();
            }

        public System.Security.SecureString Password
            {
            get
                {
                return UserPassword.SecurePassword;
                }
            }
        }
    }
