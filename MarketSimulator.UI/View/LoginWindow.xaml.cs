﻿using System.Windows;
using MarketSimulator.UI.Helpers;

namespace MarketSimulator.UI.View
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window, IHavePassword
    {
        public LoginWindow()
        {
            InitializeComponent();
        }
        public System.Security.SecureString Password
            {
            get
                {
               return LoginUserPassword.SecurePassword;
                }
            }

       
    }
}