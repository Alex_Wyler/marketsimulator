﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities;
using MarketSimulator.Entities.DataBase_Entities;
using Simulator.BL.Entities;

namespace Simulator.BL
{
    public class Simulation
    {
        private List<NPC> NPC_List; 

        public Simulation(List<User> npcList)
        {
            NPC_List = new List<NPC>();

            if (npcList == null) return;

            for (var i = 0; i < npcList.Count; i++)
            {
                NPC_List[i].NPCuser = npcList[i];
            }
        }

        public void GenerateNPC(int count)
        {
            for (int i = 0; i < count; i++)
            {
                NPC npc = new NPC(NPCtype.Generated);
            }
        }
    }
}
