﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities;
using MarketSimulator.Entities.DataBase_Entities;
using Microsoft.Win32;

namespace Simulator.BL.Entities
{
    public class NPC: INPC
    {
        public User NPCuser;

        public NPC(NPCtype type)
        {
            switch (type)
            {
                case NPCtype.Generated:
                    //NPCuser = UserFactory.GenerateUser();
                    break;

                case NPCtype.Empty:
                    //NPCuser = new User();
                    break;
            }
        }

    }


    public enum NPCtype
    {
        Generated,
        Empty
    }
}
