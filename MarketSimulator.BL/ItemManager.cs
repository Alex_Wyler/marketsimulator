﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.DA.ItemDataAcsessLayer;
using MarketSimulator.Entities.DataBase_Entities;

namespace MarketSimulator.BL
{
    public class ItemManager
    {
        public Item Item { get; set; }
        private int res;
        public List<Item> _il;

        public int UpdateStatus(int id, bool status)
        {
            res = ItemProvider.UpdateStatus(id, status);
            return res;
        }

        public int ReturnStatus()
            {
            res = ItemProvider.ReturnStatus();
            return res;
            }

        public List<Item> GetRandomItems(int userId)
        {
            _il=ItemProvider.GetRandomItems(userId);
            return _il;
        }

        public int AddNewItem(Item newItem)
        {
            var result = 0;
            ItemProvider.AddItemToBase(newItem);
            return result;
        }

        public List<Item> GetAllItems()
        {
        _il = ItemProvider.GetAllItems();
        return _il;
        }

        public int UpdateUserStory(int category, int userID)
        {
            return ItemProvider.UpdateUserStory(category, userID);
        }
        
    }
}
