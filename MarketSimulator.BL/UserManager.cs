﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using MarketSimulator.DA.UserDataAcssesLayer;
using MarketSimulator.Entities;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.Entities.Memory_Enntites;
//using MarketSimulator.Entities.Memory_Enntites.ValidationResult;
//using MarketSimulator.Entities.Memory_Enntites.ValidationResult.WarningMessage;
//using MarketSimulator.Entities.Memory_Enntites;

namespace MarketSimulator.BL
{
    public class UserManager
    {
        public User User { get; set; }
        public RegisterManager RegisterManager;
        public UnRegisterUser UnRegisterUser { get; set; }
        public ItemManager ItemManager;
        public int MainProgressBarValue { get; set; }

        private int GeneratedCode { get; set; }

        
        #region Methods


        public int ScanUser(User user)
        {
            var result = UserProvider.GetUserId(user.Login);

            return result;
        }

        public bool CheckEmailRegistration(User user)
        {
            var result = UserProvider.GetUserEmail(user.Email);
            if (String.IsNullOrEmpty(result))
            {
                return true;
            }
            else
                return false;
        }



        public ValidationResult RegisterUser(User user)
        {
            ValidationResult resultValidation = new ValidationResult();
            var resultScan = ScanUser(user);
            var resultCheckMail = CheckEmailRegistration(user);


            if (resultScan < 0 && resultCheckMail)
            {
                if (RegisterManager.AddNewUser(user))
                {
                    return resultValidation;
                }

            }
            else if (resultScan >= 0)
            {
                resultValidation.errorsList.Add(
                    new MarketSimulator.Entities.Memory_Enntites.ValidationResult.WarningMessage("Login",
                        "This Login is exists "));
            }
            else if (!resultCheckMail)
            {
                resultValidation.errorsList.Add(
                    new MarketSimulator.Entities.Memory_Enntites.ValidationResult.WarningMessage("Email",
                        "This Email is exists"));
            }

            return resultValidation;
        }

        #endregion

        #region UpdatingUser

        public int UpdateBalance(string login, decimal balance)
        {
            int result = UserProvider.UpdateBalance(login, balance);
            return result;

        }


        public RecoverySecret GetSecredQuestion(string login)
        {
            return UserProvider.GetSecredQuestionByLogin(login);
        }

        public bool SendEmailRecovery(string email)
        {
            if (UserProvider.GetUserEmail(email) == string.Empty) return false;
            var fromAddress = new MailAddress("market.simulator.test@gmail.com", "Market Simulator");
            var toAddress = new MailAddress(email, "User");
            const string fromPassword = "Admin01;Admin";
            const string subject = "Hello from Market Simulator";
            GenerateCode();
            string body = string.Format("Hello, this is your code: [ {0} ] for email-recovery", GeneratedCode);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
            User = UserProvider.LoginViaEmail(email);
            return true;
        }

        private void GenerateCode()
        {
            Random n = new Random();
            GeneratedCode = n.Next(10000, 99999);
        }

        public bool CheckValidationCode(string codeFromEmailTextBoxValue)
        {
            return Convert.ToInt32(codeFromEmailTextBoxValue) == GeneratedCode;
        }

        public void ChangePassword(string newPassword)
        {
            User.Password = UserProvider.ChangePassword(User.Id, newPassword);
        }


        #endregion

        #region Login

        public ValidationResult Login(string login, string password)
        {
            User = UserProvider.Login(login, password);
            if (User != null)
            {
                return new ValidationResult();
            }

                return new ValidationResult
                {
                    errorsList =
                        new List<ValidationResult.WarningMessage>
                        {
                            new ValidationResult.WarningMessage
                            {
                                Control = "Password",
                                Message = "Login or Password are incorrect."
                            }
                        }
                };            
        }

        public ValidationResult Login(string login)
        {
            User = UserProvider.Login(login);
            if (User != null)
            {
                return new ValidationResult();
            }

            return new ValidationResult
            {
                errorsList =
                    new List<ValidationResult.WarningMessage>
                        {
                            new ValidationResult.WarningMessage
                            {
                                Control = "Password",
                                Message = "Login or Password are incorrect."
                            }
                        }
            };
        }


        

        #endregion

        
    }



    
}