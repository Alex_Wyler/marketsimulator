﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities.Memory_Enntites
{
   public class ValidationResult
    {
       public List<WarningMessage> errorsList { get; set; }
      public  class WarningMessage
       {
          public string Control { get; set; }
          public string Message { get; set; }
           public WarningMessage()
           {
               string Control = String.Empty;
               string Message = String.Empty;
           }
           public WarningMessage(string control,string message)
           {
               Control = control;
               Message = message;
           }
       }

       public ValidationResult()
       {
           errorsList = new List<WarningMessage>();
       }
    }
}
