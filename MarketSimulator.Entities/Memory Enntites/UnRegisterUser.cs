﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities.Memory_Enntites
{
   public class UnRegisterUser
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Account { get; set; }



       public UnRegisterUser()
       {
           this.FirstName = String.Empty;
           this.LastName = String.Empty;
           this.CompanyName = String.Empty;
         
       }

       public UnRegisterUser(string login, string password, string email, string firstname, string lastname,string companyname, string account)
       {
           Login = login;
           Password = password;
           Email = email;
           FirstName = firstname;
           LastName = lastname;
           CompanyName = companyname;
           Account = account;
       }

    }
 
}
