﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities.DataBase_Entities;

namespace MarketSimulator.Entities.Memory_Enntites
{
    public class UserSearchStory
    {
        public List<SearchObject> SearchObjects { get; set; }

        public ItemCategory GetInterestingCategory()
        {
            var items = SearchObjects.Where(item => item.Date >= DateTime.Now.AddDays(-7)).Select(item => item.Category).ToList();

            Random r = new Random();
            if (items.Count > 0)
            {
                var index = r.Next(0, items.Count - 1);
                return items[index];
            }
            else
            {
                return ItemCategory.Transport;
            }          
        }
    }

    public class SearchObject
    {
        public ItemCategory Category { get; set; }
        public DateTime Date { get; set; }
    }
}
