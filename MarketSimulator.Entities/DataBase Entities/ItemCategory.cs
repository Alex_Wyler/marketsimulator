﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities.DataBase_Entities
{
    public enum ItemCategory
    {
        Transport = 6,
        Immovables = 7,
        Pets = 8,
        Electronics = 9,
        Fashion = 10,
        Entertainment = 11,
        Other = 12
    }
}
