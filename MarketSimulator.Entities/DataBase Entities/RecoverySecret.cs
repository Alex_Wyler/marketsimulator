﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities.DataBase_Entities
{
    /// <summary>
    /// Secret for special recovery.
    /// Question and answer for this question.
    /// </summary>
    public class RecoverySecret
    {
        public int Id { get; set; }

        [MaxLength(160)]
        [Required]
        public string Question{ get; set; }

        [MaxLength(160)]
        [Required]
        public string Answer { get; set; }

        public RecoverySecret()
        {
            Question = String.Empty;
            Answer = String.Empty;
        }

    }
}