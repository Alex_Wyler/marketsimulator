﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities.DataBase_Entities
{
    /// <summary>
    /// Base item class
    /// </summary>
    public class Item 
    {
        public int Id { get; set; }
        public User Owner { get; set; }
        public ItemCategory Category { get; set; }

        public bool IsValid { get; set; }
        
        public bool IsUsed { get; set; }

        [Required]
        [MaxLength(100)]
        public string Header { get; set; }

        [Required]
        [MaxLength(500)]
        public string Description { get; set; }
        public decimal Price { get; set; }
        public DateTime YearMade { get; set; }
        public bool Status { get; set; }
        public Byte[] Photo { get; set; }

        public Item(User owner, ItemCategory category, bool isUsed, string header, string description, decimal price, DateTime yearMade, bool status, Byte[] photo)
        {
            Owner = owner;
            Category = category;
            IsUsed = isUsed;
            Header = header;
            Description = description;
            Price = price;
            YearMade = yearMade;
            Status = status;
            Photo = photo;
            IsValid = true;
        }

        public Item()
        {
            Header = String.Empty;
            Description = String.Empty;
            Price = 0;
            IsValid = true;
        }
}
}
