﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities.Memory_Enntites;
using System.Drawing;

namespace MarketSimulator.Entities.DataBase_Entities
{
    public class User
    {
        public int Id { get; set; }

        public string SecretAnswer { get; set; }
        public string SecretQuestion { get; set; }

        [Required]
        [MaxLength(50)]
        public string Login { get; set; }

        [Required]
        [MaxLength(50)]
        public string Password { get; set; } 

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; } 

        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        [Required]
        [MaxLength(50)]
        public string Email { get; set; } 
        public string BankAccount { get; set; }
        public decimal Balance { get; set; }
        public RecoverySecret Secret { get; set; }

        [Required]
        [MaxLength(50)]
        public string CompanyName { get; set; } 
        
        public List<Item> Items { get; set; }

        //public User(int ID, bool IsUser,string FirstName,string LastName,string PhoneNumber,UserAdress Adress, RecoverySecret Secret,
        //    string CompanyName,BankAccount BankAccount,byte Age,string Login,string Password,string Email)
        //{
        //    Id = ID;
        //    this.IsUser = IsUser;
        //    this.FirstName = FirstName;
        //    this.LastName = LastName;
        //    this.PhoneNumber = PhoneNumber;
        //    this.Adress = Adress;
        //    this.Secret = Secret;
        //    this.CompanyName = CompanyName;
        //    this.BankAccount = BankAccount;
        //    this.Age = Age;
        //    this.Login = Login;
        //    this.Password = Password;
        //    this.Email = Email;
        //}

        public User(string login, string password, string email, string name, string surName, string organization, string account, string secretquestion, string secretanswer)
        {
            Login = login;
            Password = password;
            Email = email;
            FirstName = name;
            LastName = surName;
            CompanyName = organization;
            BankAccount = account;
            SecretQuestion = secretquestion;
            SecretAnswer = secretanswer;

        }

        public User(UnRegisterUser unregUser)
        {
            this.Login = unregUser.Login;
            this.Password = unregUser.Password;
            this.Email = unregUser.Email;
            this.FirstName = unregUser.FirstName;
            this.LastName = unregUser.LastName;
            this.CompanyName = unregUser.CompanyName;

            //this.PhoneNumber = String.Empty;
            //this.Adress=new UserAdress();
            //this.Secret=new RecoverySecret();
            //this.BankAccount=new BankAccount();
            //this.Age = 0;


        }

        public User()
        {
            Login = String.Empty;
            Password = String.Empty;
            FirstName = String.Empty;
            LastName = String.Empty;
            Email = String.Empty;
            BankAccount = String.Empty; 
            Balance = 0;
            CompanyName = String.Empty;
        }
    }
    
}

