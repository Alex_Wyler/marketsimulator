﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities
{
    public enum TransportType
    {
        Land = 0,
        Water = 1,
        Sky = 2
    }
}
