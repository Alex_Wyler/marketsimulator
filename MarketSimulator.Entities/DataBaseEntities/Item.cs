﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketSimulator.Entities
{
    public abstract class Item
    {
        public string Id{ get; set;}
        public string Description{ get; set;}
        public bool IsUsed { get; set; }
        public decimal Price { get; set; }
        public string Owner { get; set; }
    }
}
