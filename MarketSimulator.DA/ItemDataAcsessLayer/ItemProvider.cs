﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MarketSimulator.Entities;
using MarketSimulator.Entities.DataBase_Entities;
using MarketSimulator.Entities.Memory_Enntites;

namespace MarketSimulator.DA.ItemDataAcsessLayer
{
    public static class ItemProvider
    {
        /// <summary>
        /// Methods for work in item table from database
        /// Methods "Get" allow to take values from the table
        /// </summary>

        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static User GetItemOwner(int id)
        {
            User result = new User();
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE ID = '{0}'", id);
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    result.Id = (int)reader["Id"];
                    result.Login = (string)reader["Login"];
                    result.Password = (string)reader["Password"];
                    result.FirstName = (string)reader["FirstName"];
                    result.LastName = (string)reader["LastName"];
                    result.Balance = (decimal)reader["Balance"];
                    result.BankAccount = (string)reader["BankAccount"];
                    result.Email = (string)reader["Email"];
                    result.CompanyName = (string)reader["CompanyName"];

                }
                else
                {
                    result = null;
                }
                reader.Close();
            }
            return result;
        }
        public static List<Item> GetAllItems()
            {
            string sqlExpression = "sp_GetAllItems";
            List<Item> listItems = new List<Item>();

            using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
               SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    {

                    listItems.Add(new Item
                    {
                        Id = (int)reader["Id"],
                        Owner = GetItemOwner((int)reader["Owner_ID"]),
                        IsUsed = (bool)reader["IsUsed"],
                        Header = (string)reader["Header"],
                        Description = (string)reader["Description"],
                        Price = (decimal)reader["Price"],
                        YearMade = (DateTime)reader["YearMade"],
                        Category = (ItemCategory)(int)reader["ItemCategory_ID"],
                        Status = (bool)reader["Status"],
                        Photo = (byte[])reader["Photo"]
                    });

                    }
                reader.Close();

                }

            return listItems;
            }

        public static List<Item> GetRandomItems(int userId)
        {
            var result = new List<Item>();
            List<Item> listItems = new List<Item>();
            var story = new UserSearchStory() {SearchObjects = new List<SearchObject>()};
            
            var sqlExpression = $"SELECT * FROM [UserStory] WHERE User_ID = {userId}";
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpression, connection);

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    story.SearchObjects.Add(new SearchObject()
                    {
                        Category = (ItemCategory)(int)reader["Category_ID"],
                        Date = Convert.ToDateTime(reader["Date"])
                    });
                }
                reader.Close();

                int category = (int)story.GetInterestingCategory();

                sqlExpression =
                    $"SELECT TOP 9 * FROM [Item] WHERE ItemCategory_ID={category}";
                    command = new SqlCommand(sqlExpression, connection);

                reader = command.ExecuteReader();
                while (reader.Read())
                {

                    listItems.Add(new Item
                    {
                        Id = (int)reader["Id"],
                        Owner = GetItemOwner((int)reader["Owner_ID"]),
                        IsUsed = (bool)reader["IsUsed"],
                        Header = (string)reader["Header"],
                        Description = (string)reader["Description"],
                        Price = (decimal)reader["Price"],
                        YearMade = (DateTime)reader["YearMade"],
                        Photo = (byte[])reader["Photo"],
                        Status = (bool)reader["Status"],
                        Category = (ItemCategory)(int)reader["ItemCategory_ID"]
                    });

                }
                reader.Close();
            }
            return listItems;
        }

         

        public static int AddItemToBase(Item item)
        {
            int result = -1;

            string sqlExpression = "sp_InsertItem";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection) { CommandType = CommandType.StoredProcedure };

                command.Parameters.Add("@owner", SqlDbType.Int).Value = item.Owner.Id;
                command.Parameters.Add("@category", SqlDbType.Int).Value = item.Category;
                command.Parameters.Add("@header",SqlDbType.VarChar).Value=item.Header;
                command.Parameters.Add("@description",SqlDbType.VarChar).Value=item.Description;
                command.Parameters.Add("@price",SqlDbType.Decimal).Value=item.Price;
                command.Parameters.Add("@yearMade",SqlDbType.DateTime).Value=item.YearMade;
                command.Parameters.Add("@isUsed", SqlDbType.Bit).Value = item.IsUsed;
                command.Parameters.Add("@status", SqlDbType.Bit).Value = item.Status;
                command.Parameters.Add("@photo", SqlDbType.VarBinary).Value = item.Photo;

                result = Convert.ToInt32(command.ExecuteScalar());
            }
            return result;
        }


        public static int ReturnStatus()
            {
            int result = -1;
            string returnStatus = "sp_ReturnStatus";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                connection.Open();
                SqlCommand command = new SqlCommand(returnStatus, connection);

                command.CommandType = CommandType.StoredProcedure;

                result = Convert.ToInt32(command.ExecuteScalar());
                }
            return result;
            }

        public static int UpdateStatus(int id, bool status)
            {
            int result = -1;
            string updateStatus = "sp_UpdateStatus";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                connection.Open();
                SqlCommand command = new SqlCommand(updateStatus, connection);

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter idParameter = new SqlParameter
                {
                    ParameterName = "@id",
                    Value = id
                };
                command.Parameters.Add(idParameter);

                SqlParameter statusParameter = new SqlParameter
                {
                    ParameterName = "@status",
                    Value = status
                };
                command.Parameters.Add(statusParameter);

                result = Convert.ToInt32(command.ExecuteScalar());
                }
            return result;
            }

        public static int UpdateUserStory(int category, int userId)
        {
            var result = -1;
            
            var sqlExpression =
                $"INSERT INTO [UserStory] (User_ID, Category_ID, [Date]) VALUES ('{userId}', '{category}', '{DateTime.Now.ToString(CultureInfo.InvariantCulture)}'); SELECT SCOPE_IDENTITY()";
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpression, connection);
              result = Convert.ToInt32(command.ExecuteScalar());
            }
            return result;
        }
    }
}
