﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using MarketSimulator.Entities.DataBase_Entities;

namespace MarketSimulator.DA.UserDataAcssesLayer
{
    public static class UserProvider
    {
        /// <summary>
        /// Methods for work in user table from database
        /// Methods "Get" allow to take values from the tables
        /// </summary>

        static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #region Get methods

        public static int GetUserId(string login)
        {
            int userId = -1;
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Login = '{0}'", login);
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        userId = (int)reader[0];

                    }
                    reader.Close();
                }

            }
            catch (Exception)
            {
                return userId;
            }
            return userId;
        }

        public static string GetUserEmail(string email)
        {
            string result = String.Empty;
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Email = '{0}'", email);
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        result = (string)reader["Email"];

                    }
                    reader.Close();
                }

            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public static User Login(string login, string password)
        {
            User result = new User();
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Login = '{0}' AND Password ='{1}'", login, password);
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        result.Id = (int)reader["Id"];
                        result.Login = (string)reader["Login"];
                        result.Password = (string)reader["Password"];
                        result.FirstName = (string)reader["FirstName"];
                        result.LastName = (string)reader["LastName"];
                        result.Balance = (decimal) reader["Balance"];
                        result.BankAccount = (string) reader["BankAccount"];
                        result.Email = (string)reader["Email"];
                        result.CompanyName = (string)reader["CompanyName"];

                    }
                    else
                    {
                        result = null;
                    }
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }

        public static User Login(string login)
        {
            User result = new User();
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Login = '{0}'", login);
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        result.Id = (int)reader["Id"];
                        result.Login = (string)reader["Login"];
                        result.Password = (string)reader["Password"];
                        result.FirstName = (string)reader["FirstName"];
                        result.LastName = (string)reader["LastName"];
                        result.Balance = (decimal)reader["Balance"];
                        result.BankAccount = (string)reader["BankAccount"];
                        result.Email = (string)reader["Email"];
                        result.CompanyName = (string)reader["CompanyName"];

                    }
                    else
                    {
                        result = null;
                    }
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }


        public static User LoginViaEmail(string email)
        {
            User result = new User();
            string sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Email = '{0}'", email);
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpressionResult, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    reader.Read();
                    if (reader.HasRows)
                    {
                        result.Id = (int)reader["Id"];
                        result.Login = (string)reader["Login"];
                        result.Password = (string)reader["Password"];
                        result.FirstName = (string)reader["FirstName"];
                        result.LastName = (string)reader["LastName"];
                        result.Balance = (decimal)reader["Balance"];
                        result.BankAccount = (string)reader["BankAccount"];
                        result.Email = (string)reader["Email"];
                        result.CompanyName = (string)reader["CompanyName"];

                    }
                    else
                    {
                        result = null;
                    }
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return result;
            }
            return result;
        }


        #endregion

        #region Insert methods

        public static int AddRecoverySecret(string secretquestion, string secretanswer)
        {
        int result = -1;
        string sqlExpression = "sp_InsertRecoverySecret";

        using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
            connection.Open();
            SqlCommand command = new SqlCommand(sqlExpression, connection)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };

            SqlParameter questionParameter = new SqlParameter
            {
                ParameterName = "@question",
                Value = secretquestion
            };
            command.Parameters.Add(questionParameter);

            SqlParameter answerParameter = new SqlParameter
            {
                ParameterName = "@answer",
                Value = secretanswer
            };
            command.Parameters.Add(answerParameter);

            result = Convert.ToInt32(command.ExecuteScalar());
            }

        return result;  
        }

        public static int AddUserToBase(User user)
        {
            int result = -1;
            int secretId = AddRecoverySecret(user.SecretQuestion, user.SecretAnswer);
            if (secretId != -1)
            {

                string sqlExpression = "sp_InsertUser";

                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(sqlExpression, connection)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    SqlParameter loginParameter = new SqlParameter
                    {
                        ParameterName = "@login",
                        Value = user.Login
                    };
                    command.Parameters.Add(loginParameter);

                    SqlParameter passwordParameter = new SqlParameter
                    {
                        ParameterName = "@password",
                        Value = user.Password
                    };
                    command.Parameters.Add(passwordParameter);

                    SqlParameter firstNameParameter = new SqlParameter
                    {
                        ParameterName = "@firstname",
                        Value = user.FirstName
                    };
                    command.Parameters.Add(firstNameParameter);

                    SqlParameter lastNameParameter = new SqlParameter
                    {
                        ParameterName = "@lastname",
                        Value = user.LastName
                    };
                    command.Parameters.Add(lastNameParameter);

                    SqlParameter emailParameter = new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = user.Email
                    };
                    command.Parameters.Add(emailParameter);

                    SqlParameter companyNameParameter = new SqlParameter
                    {
                        ParameterName = "@companyname",
                        Value = user.CompanyName
                    };
                    command.Parameters.Add(companyNameParameter);

                    SqlParameter accountParameter = new SqlParameter
                    {
                        ParameterName = "@account",
                        Value = user.BankAccount
                    };
                    command.Parameters.Add(accountParameter);

                    SqlParameter secretIdParameter = new SqlParameter
                    {
                        ParameterName = "@secretid",
                        Value = secretId
                    };
                    command.Parameters.Add(secretIdParameter);

                    SqlParameter balanceParameter = new SqlParameter
                    {
                        ParameterName = "@balance",
                        Value = 0
                    };
                    command.Parameters.Add(balanceParameter);


                    result = Convert.ToInt32(command.ExecuteScalar());
                }

                return result;
            }
            else return -1;
        }
        
        public static RecoverySecret GetSecredQuestionByLogin(string login)
        {
            RecoverySecret result = new RecoverySecret();
            var recoverySecretId = -1;
            var sqlExpressionResult = string.Format("SELECT * FROM Users WHERE Login = '{0}'", login);
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                var command = new SqlCommand(sqlExpressionResult, connection);
                var reader = command.ExecuteReader();
                reader.Read();
                if (!reader.HasRows)
                {
                    throw new Exception("Login not found");
                }
                result.Id = (int)reader["RecoverySecret_ID"];
                reader.Close();

                sqlExpressionResult = string.Format("SELECT * FROM RecoverySecret WHERE Id = '{0}'", result.Id);
                command = new SqlCommand(sqlExpressionResult, connection);
                reader = command.ExecuteReader();
                reader.Read();
                result.Question = (string) reader["Question"];
                result.Answer = (string)reader["Answer"];

            }
            return result;
        }

        #endregion

        #region Update methods

        public static string ChangePassword(int id, string newPassword)
        {
            string sqlExpression = string.Format("UPDATE Users SET Password='{0}' WHERE Id='{1}'", newPassword, id);
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                reader.Close();
            }
            return newPassword;
        }

        public static int UpdateBalance(string login, decimal balance)
            {
            int result = -1;
            string updatePassword = "sp_UpdateBalance";

            using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                connection.Open();
                SqlCommand command = new SqlCommand(updatePassword, connection);

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter loginParameter = new SqlParameter
                {
                    ParameterName = "@login",
                    Value = login,
                };
                command.Parameters.Add(loginParameter);

                SqlParameter balanceParameter = new SqlParameter
                {
                    ParameterName = "@balance",
                    Value = balance,
                };
                command.Parameters.Add(balanceParameter);

                result = Convert.ToInt32(command.ExecuteScalar());
                }
            return result;
            }

        #endregion
        
    }
}
